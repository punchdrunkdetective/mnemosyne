﻿using mnemosyne.muse;
using System;

namespace mnemosyne.passwordgenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            var passwordManager = new PasswordManager();
            while (true)
            {
                var line = Console.ReadLine();
                var password = passwordManager.EncodePassword(line);
                Console.WriteLine(password);
            }
        }
    }
}
