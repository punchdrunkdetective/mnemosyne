﻿namespace mnemosyne.muse
{
    public interface IPasswordManager
    {
        public string EncodePassword(string password);
        public bool VerifyPassword(string encodedPassword, string password);
    }
}
