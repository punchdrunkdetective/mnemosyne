﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mnemosyne.muse
{
    [Index(nameof(Username), Name = "IX_User_Username", IsUnique = true)]
    [Index(nameof(Email), Name = "IX_User_Email", IsUnique = true)]
    public class User
    {
        public User()
        {
            Username = "";
            Password = "";
            Role = "";
        }

        public User(User u)
        {
            if (u == null) throw new ArgumentNullException("Attempt to copy from null User.", nameof(u));

            Id = u.Id;
            Username = u.Username;
            Password = u.Password;
            Role = u.Role;
            LastLogin = u.LastLogin;
            GivenName = u.GivenName;
            Surname = u.Surname;
            Email = u.Email;

            if (u.CreatedBy != null)
            {
                CreatedBy = new UserCreatedBy(u.CreatedBy);
                CreatedBy.NewUser = this;
            }

            if (u.EditedBy != null)
            {
                EditedBy = new UserEditedBy(u.EditedBy);
                EditedBy.ModifiedUser = this;
            }

            if (u.DeletedBy != null)
            {
                DeletedBy = new UserDeletedBy(u.DeletedBy);
                DeletedBy.RemovedUser = this;
            }

            if (u.Ratings != null)
            {
                Ratings = new List<Rating>();
                u.Ratings.ForEach(r => {
                    Ratings.Add(r);
                    r.User = this;
                });
            }
        }

        public int Id { get; set; }

        /// <summary>
        /// Username used to login. Must be nonnull.
        /// </summary>
        [Required]
        [Remote(action: "VerifyUsername", controller: "Users")]
        public string Username {
            get { return _username; }
            set {
                _username = value?.Trim() ?? throw new ArgumentException($"{nameof(Username)} must have nonnull value."); 
            }
        }
        private string _username;

        /// <summary>
        /// Password used to login. Must be nonnull.
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Remote(action: "VerifyPassword", controller: "Users")]
        public string Password {
            get { return _password; } 
            set {
                _password = value?.Trim() ?? throw new ArgumentException($"{nameof(Password)} must have nonnull value.");
            }
        }
        private string _password;

        /// <summary>
        /// Role used in authorization policies. Must be nonnull.
        /// </summary>
        [DisplayFormat(ConvertEmptyStringToNull = false)] // null will blow up the policy code
        public string Role { 
            get { return _role; }
            set {
                _role = value?.Trim() ?? throw new ArgumentException($"{nameof(Role)} must have nonnull value.");
            } 
        }
        private string _role;

        public DateTime? LastLogin { get; set; }

        [Display(Name = "First Name")]
        public string GivenName { 
            get { return _givenName; } 
            set {
                _givenName = value?.Trim();
            }
        }
        private string _givenName;

        [Display(Name = "Last Name")]
        public string Surname { 
            get { return _surName; }
            set {
                _surName = value?.Trim();
            }
        }
        private string _surName;

        [NotMapped]
        public string InformalName
        {
            get
            {
                return !string.IsNullOrWhiteSpace(GivenName) ? GivenName : Username;
            }
        }

        [DataType(DataType.EmailAddress)]
        [Remote(action: "VerifyEmail", controller: "Users")]
        public string Email { 
            get { return _email; }
            set {
                _email = value?.Trim();
            }
        }
        private string _email;

        public List<Rating> Ratings { get; set; }

        public UserCreatedBy CreatedBy { get; set; }

        public UserEditedBy EditedBy { get; set; }

        public UserDeletedBy DeletedBy { get; set; }
    }
}
