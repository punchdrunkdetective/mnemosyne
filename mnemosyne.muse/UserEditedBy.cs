﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class UserEditedBy
    {
        public UserEditedBy()
        { }

        public UserEditedBy(UserEditedBy eb)
        {
            if (eb == null) throw new ArgumentNullException("Attempt to copy from null UserEditedBy.", nameof(eb));

            Id = eb.Id;
            EditorId = eb.EditorId;
            Date = eb.Date;
            ModifiedUserId = eb.ModifiedUserId;
            ModifiedUser = eb.ModifiedUser;
        }

        public int Id { get; set; }

        [Required]
        public int EditorId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int ModifiedUserId { get; set; }

        public User ModifiedUser { get; set; }
    }
}
