﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class UserDeletedBy
    {
        public UserDeletedBy() { }

        public UserDeletedBy(UserDeletedBy db)
        {
            if (db == null) throw new ArgumentNullException("Attempt to copy from null UserDeleteddBy.", nameof(db));

            Id = db.Id;
            DeletorUserid = db.DeletorUserid;
            Date = db.Date;
            RemovedUserId = db.RemovedUserId;
            RemovedUser = db.RemovedUser;
        }

        public int Id { get; set; }

        [Required]
        public int DeletorUserid { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int RemovedUserId { get; set; }

        public User RemovedUser { get; set; }
    }
}
