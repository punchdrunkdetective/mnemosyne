﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class UserCreatedBy
    {
        public UserCreatedBy()
        {}

        public UserCreatedBy(UserCreatedBy cb)
        {
            if (cb == null) throw new ArgumentNullException("Attempt to copy from null UserCreatedBy.", nameof(cb));

            Id = cb.Id;
            Date = cb.Date;
            CreatorId = cb.CreatorId;
            NewUserId = cb.NewUserId;
            NewUser = cb.NewUser;
        }

        public int Id { get; set; }

        [Required]
        public int CreatorId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int NewUserId { get; set; }

        public User NewUser { get; set; }
    }
}
