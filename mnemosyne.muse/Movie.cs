﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace mnemosyne.muse
{
    public class Movie
    {
        public Movie() 
        {
            Title = "";
        }

        public Movie(Movie m)
        {
            if (m == null) throw new ArgumentNullException("Attempt to copy from null Movie.", nameof(m));

            Id = m.Id;
            Title = m.Title;
            Director = m.Director;
            ReleaseYear = m.ReleaseYear;
            Genre = m.Genre;
            Watched = m.Watched;

            if (m.CreatedBy != null)
            {
                CreatedBy = new MovieCreatedBy(m.CreatedBy);
                CreatedBy.Movie = this;
            }

            if (m.EditedBy != null)
            {
                EditedBy = new MovieEditedBy(m.EditedBy);
                EditedBy.Movie = this;
            }

            if (m.DeletedBy != null)
            {
                DeletedBy = new MovieDeletedBy(m.DeletedBy);
                DeletedBy.Movie = this;
            }

            if (m.Ratings != null)
            {
                Ratings = new List<Rating>();
                m.Ratings.ForEach(r => { 
                    Ratings.Add(r);
                    r.Movie = this;
                });
            }
        }

        public int Id { get; set; }

        /// <summary>
        /// Title of the movie. Must be nonnull.
        /// </summary>
        [Required]
        public string Title {
            get { return _title; }
            set {
                _title = value?.Trim() ?? throw new ArgumentException($"{nameof(Title)} must have nonnull value.");
            }
        }
        private string _title;

        public string Director { 
            get { return _director; }
            set {
                _director = value?.Trim();
            }
        }
        private string _director;

        /// <summary>
        /// Year in which the movie was released. Cannot be negative. 
        /// </summary>
        [Range(0, Int32.MaxValue, ErrorMessage = "The field Release Year must be a valid year.")]
        public int? ReleaseYear { 
            get { return _releaseYear; }
            set {
                if (value < 0) throw new ArgumentException($"{nameof(ReleaseYear)} must have non negative value.");
                _releaseYear = value;
            }
        }
        private int? _releaseYear;

        public Genre Genre { get; set; }

        public bool Watched { get; set; }

        public string Poster { get; set; }

        [NotMapped]
        public double AverageRating { get
            {
                if (Ratings == null || Ratings.Count == 0)
                {
                    return 0;
                }

                var average = Ratings
                    .Where(r => r != null)
                    .Average(r => r.Value);
                average = Math.Truncate(average * 10) / 10; 

                return average;
            } 
        }

        public List<Rating> Ratings { get; set; }

        public MovieCreatedBy CreatedBy { get; set; }

        public MovieEditedBy EditedBy { get; set; }
        
        public MovieDeletedBy DeletedBy { get; set; }
    }
}
