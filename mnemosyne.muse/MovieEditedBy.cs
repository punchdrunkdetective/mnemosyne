﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class MovieEditedBy
    {
        public MovieEditedBy()
        {
        }

        public MovieEditedBy(MovieEditedBy eb)
        {
            if (eb == null) throw new ArgumentNullException("Attempt to copy from MovieEditedBy.");

            Id = eb.Id;
            UserId = eb.UserId;
            Date = eb.Date;
            MovieId = eb.MovieId;
            Movie = eb.Movie;
        }

        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int MovieId { get; set; }

        public Movie Movie { get; set; }
    }
}
