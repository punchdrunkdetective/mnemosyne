﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class MovieDeletedBy
    {
        public MovieDeletedBy()
        {
        }

        public MovieDeletedBy(MovieDeletedBy db)
        {
            if (db == null) throw new ArgumentNullException("Attempt to copy from null MovieDeletedBy.", nameof(db));

            Id = db.Id;
            UserId = db.UserId;
            Date = db.Date;
            MovieId = db.MovieId;
            Movie = db.Movie;
        }

        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int MovieId { get; set; }

        public Movie Movie { get; set; }
    }
}
