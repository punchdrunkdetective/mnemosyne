﻿using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public enum Genre
    {
        Action = 1,
        Adventure,        
        Comedy,
        Crime,
        Drama,
        Experimental,
        Fantasy,
        Historical,
        Horror,
        [Display(Name = "Martial Arts")]
        MartialArts,
        Musical,
        Mystery,
        [Display(Name = "Science Fiction")]
        ScienceFiction,
        Sport,
        Superhero,
        Thriller,
        War,
        Western,        
        Other = 0
    }
}
