﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Linq;
using System.Security.Cryptography;

namespace mnemosyne.muse
{
    public class PasswordManager : IPasswordManager
    {
        //NOTE: If mnemosyne.data turns out to be the only thing that uses this class, as an implementation detail of authenticating or registering
        //      a user, then it might be best to move this code to that project. 

        private readonly KeyDerivationPrf _prf = KeyDerivationPrf.HMACSHA1;
        private readonly int _saltSize = 16; //bytes
        private readonly int _hashSize = 32; //bytes
        private readonly int _iterations = 10000;

        /// <summary>
        /// Encodes a plain text password with a salt, hash, and iteration count. 
        /// </summary>
        /// <param name="password">A non null, non empty value.</param>
        /// <returns>Password encoded in the format salt.hash.interations </returns>
        /// <exception cref="ArgumentNullException">If password is null.</exception>
        /// <exception cref="ArgumentException">If password is empty.</exception>
        public string EncodePassword(string password)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException(nameof(password));

            var salt = GetSalt();
            var hash = GetHash(_iterations, salt, password);

            return $"{Convert.ToBase64String(salt)}.{Convert.ToBase64String(hash)}.{_iterations}";
        }

        /// <summary>
        /// Tests a password against an encoded password to verify they are equal.
        /// </summary>
        /// <param name="encodedPassword">A password encoded as salt.hash.iterations </param>
        /// <param name="password">A password to test against encodedPassword for equality.</param>
        /// <returns>True if encoded password is equal to password, otherwise false.</returns>
        /// <exception cref="ArgumentNullException">If encodedPassword or password is null.</exception>
        /// <exception cref="ArgumentException">If encodedPassword or password is empty.</exception>
        /// <exception cref="ArgumentException">If encodedPassword is not of the format salt.hash.iterations </exception>
        public bool VerifyPassword(string encodedPassword, string password)
        {
            if (encodedPassword == null) throw new ArgumentNullException(nameof(password));
            if (password == null) throw new ArgumentNullException(nameof(password));

            if (string.IsNullOrWhiteSpace(encodedPassword)) throw new ArgumentException(nameof(encodedPassword));
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException(nameof(password));

            var (salt, hash, iterations) = DecodePassword(encodedPassword);
            var guess = GetHash(iterations, salt, password);

            return hash.SequenceEqual(guess);
        }

        private byte[] GetSalt()
        {
            byte[] salt = new byte[_saltSize];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(salt);
            }
            return salt;
        }

        private byte[] GetHash(int iterations, byte[] salt, string password)
        {
            return KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: _prf,
                iterationCount: iterations,
                numBytesRequested: _hashSize);
        }

        private (byte[], byte[], int) DecodePassword(string password)
        {
            try
            {
                var parts = password.Split(".");
                var salt = Convert.FromBase64String(parts[0]);
                var hash = Convert.FromBase64String(parts[1]);
                var iterations = int.Parse(parts[2]); //TODO: will want to check with _iterations to see if new has is needed
            
                return (salt, hash, iterations);
            }
            catch(Exception ex)
            {
                throw new ArgumentException("Failed to decode password.", ex);
            }            
        }
    }
}
