﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class Rating
    {
        public const int Max = 5;

        public Rating() { }

        public Rating(Rating r)
        {
            if (r == null)
            {
                throw new ArgumentNullException("Rating must not be null.", nameof(r));
            }

            Id = r.Id;
            MovieId = r.MovieId;
            UserId = r.UserId;
            Value = r.Value;
            Movie = r.Movie;
            User = r.User;
        }

        public int Id { get; set; }

        [Required]
        public int MovieId { get; set; }

        public Movie Movie { get; set; }

        [Required]
        public int UserId { get; set; }

        public User User { get; set; }

        [Required]
        [Range(0, 5, ErrorMessage = "The field Rating must be between 0 and 5.")]
        public int Value
        {
            get { return _value; }
            set
            {
                _value = value >= 0 && value <= Max 
                    ? value 
                    : throw new ArgumentException($"Rating must be between 0 and 5.");
            }
        }
        private int _value;
    }
}
