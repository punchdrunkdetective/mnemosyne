﻿using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.muse
{
    public class MovieCreatedBy
    {
        public MovieCreatedBy()
        {
        }

        public MovieCreatedBy(MovieCreatedBy cb)
        {
            if (cb == null) throw new ArgumentNullException("Attempt to copy from null MovieCreatedBy.", nameof(cb));

            Id = cb.Id;
            UserId = cb.UserId;
            Date = cb.Date;
            MovieId = cb.MovieId;
            Movie = cb.Movie;
        }

        public int Id { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public int MovieId { get; set; }

        public Movie Movie { get; set; }
    }
}
