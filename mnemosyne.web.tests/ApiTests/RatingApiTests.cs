﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.muse;
using mnemosyne.web.Api;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.Mocks;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.ApiTests
{
    public class RatingApiTests
    {
        private readonly RatingsController _api;
        private readonly UserManagerMock _userManagerMock;
        private readonly MovieDataMock _movieDataMock;
        private readonly RatingDataMock _ratingDataMock;

        public RatingApiTests()
        {
            _userManagerMock = new UserManagerMock();
            _movieDataMock = new MovieDataMock();
            _ratingDataMock = new RatingDataMock();
            _api = new RatingsController(_userManagerMock.Object, _movieDataMock.Object, _ratingDataMock.Object);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(3)]
        [InlineData(5)]
        public void RateMovie_WithValidInput_WithNewRating_ShouldSetRating(int rating)
        {
            var movieId = 1;

            _ratingDataMock
                .Setup(m => m.GetById(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(() => null);

            var expectedRating = new Rating()
            {
                UserId = _userManagerMock.actorId,
                MovieId = movieId,
                Value = rating,
            };

            Rating actualRating = null;
            _ratingDataMock
                .Setup(m => m.Create(It.IsAny<Rating>()))
                .Returns<Rating>(r =>
                {
                    actualRating = r;
                    return r;
                });

            var result = _api.RateMovie(movieId, rating);

            TestHelper.AssertEqual(expectedRating, actualRating);
            _ratingDataMock.Verify(m => m.Create(It.IsAny<Rating>()), Times.Once);
            _ratingDataMock.Verify(m => m.Update(It.IsAny<Rating>()), Times.Never);
            _ratingDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(3)]
        [InlineData(5)]
        public void RateMovie_WithValidInput_WithExistingRating_ShouldSetRating(int rating)
        {
            var movieId = 1;

            var expectedRating = new Rating()
            {
                UserId = _userManagerMock.actorId,
                MovieId = movieId,
                Value = rating,
            };

            Rating actualRating = null;
            _ratingDataMock
                .Setup(m => m.Update(It.IsAny<Rating>()))
                .Returns<Rating>(r =>
                {
                    actualRating = r;
                    return r;
                });

            var result = _api.RateMovie(movieId, rating);

            TestHelper.AssertEqual(expectedRating, actualRating);
            _ratingDataMock.Verify(m => m.Create(It.IsAny<Rating>()), Times.Never);
            _ratingDataMock.Verify(m => m.Update(It.IsAny<Rating>()), Times.Once);
            _ratingDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void RateMovie_WithValidInput_ShouldReturnStatusOk()
        {
            var result = _api.RateMovie(1, 5);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void RateMovie_WithInvalidMovieId_ShouldNotSetRating()
        {
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _api.RateMovie(1, 5);
            _ratingDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void RateMovie_WithInvalidMovieId_ShouldReturnStatusNotFound()
        {
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _api.RateMovie(1, 5);
            Assert.IsType<NotFoundResult>(result);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(6)]
        public void RateMovie_WithInvalidRating_ShouldNotSetRating(int rating)
        {
            var result = _api.RateMovie(1, rating);
            _ratingDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(6)]
        public void RateMovie_WithInvalidRating_ShouldReturnBadRequest(int rating)
        {
            var result = _api.RateMovie(1, rating);
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public void RateMovie_WithDbError_ShouldReturnStatusInternalServerError()
        {
            _ratingDataMock
                .Setup(m => m.GetById(It.IsAny<int>(), It.IsAny<int>()))
                .Callback(() => throw new Exception());

            var result = _api.RateMovie(1, 5);

            var codeResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(500, codeResult.StatusCode);
        }

        [Fact]
        public void RateMovie_WithMovieDeleted_ShouldReturnBadRequest()
        {
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            var result = _api.RateMovie(1, 5);

            Assert.IsType<BadRequestResult>(result);
        }
    }
}
