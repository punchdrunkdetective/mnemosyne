﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.muse;
using mnemosyne.web.Api;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.Mocks;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.ApiTests
{
    public class MovieApiTests
    {
        private readonly MoviesController _api;
        private readonly MovieDataMock _movieDataMock;

        public MovieApiTests()
        {
            _movieDataMock = new MovieDataMock();
            _api = new MoviesController(_movieDataMock.Object);
        }

        [Fact]
        public void DeleteMovie_WithValidMovieId_ShouldDeleteMovie()
        {
            var movieId = 1;
            var result = _api.DeleteMovie(movieId);
            _movieDataMock.Verify(m => m.Update(It.IsAny<Movie>()), Times.Once);
            _movieDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void DeleteMovie_WithValidMovieid_ShouldSetDeletedByUser()
        {
            Assert.Null(_movieDataMock.testMovie.DeletedBy);
            var movieId = 1;
            _api.DeleteMovie(movieId);
            Assert.Equal(-1 , _movieDataMock.testMovie.DeletedBy.UserId); //TODO: add authentication
        }

        [Fact]
        public void DeleteMovie_WithValidMovieid_ShouldSetDeletedByDate()
        {
            Assert.Null(_movieDataMock.testMovie.DeletedBy);
            var movieId = 1;
            var now = DateTime.Now;
            _api.DeleteMovie(movieId);
            TestHelper.AssertEqual(now, _movieDataMock.testMovie.DeletedBy.Date);
        }

        [Fact]
        public void DeleteMovie_WithValidMovieid_ShouldReturnStatusOk()
        {
            var movieId = 1;
            var result = _api.DeleteMovie(movieId);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public void DeleteMovie_WithInvalidMovieId_ShouldNotDeleteMovie()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _api.DeleteMovie(movieId);
            _movieDataMock.Verify(m => m.Update(It.IsAny<Movie>()), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void DeleteMovie_WithInvalidMovieid_ShouldNotSetDeletedByRecord()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _api.DeleteMovie(movieId);
            Assert.Null(_movieDataMock.testMovie.DeletedBy);
        }

        [Fact]
        public void DeleteMovie_WithInvalidMovieid_ShouldReturnStatusNotFound()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);
            var result = _api.DeleteMovie(movieId);
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void DeleteMovie_WithDbError_ShouldReturnStatusInternalServerError()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() => throw new Exception());

            var result = _api.DeleteMovie(movieId);

            var codeResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(500, codeResult.StatusCode);
        }

        [Fact]
        public void DeleteMovie_WithMovieDeleted_ShouldNotDeleteMovie()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            _api.DeleteMovie(movieId);

            _movieDataMock.Verify(m => m.Update(It.IsAny<Movie>()), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void DeleteMovie_WithMovieDeleted_ShouldReturnStatusBadRequest()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            var result = _api.DeleteMovie(movieId);

            var codeResult = Assert.IsType<BadRequestResult>(result);
            Assert.Equal(400, codeResult.StatusCode);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void MarkMovieWatched_WithValidInput_ShouldMarkMovie(bool watched)
        {
            const int movieId = 1234;
            _movieDataMock.testMovie.Watched = !watched;

            _api.MarkeMovieWatched(movieId, watched);

            _movieDataMock.Verify(m => m.Commit(), Times.Once);
            Assert.Equal(watched, _movieDataMock.testMovie.Watched);
        }

        [Fact]
        public void MarkMovieWatched_WithInvalidMovie_ShouldNotMarkMovie()
        {
            _movieDataMock.testMovie.Watched = false;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _api.MarkeMovieWatched(1234, true);

            _movieDataMock.Verify(m => m.Commit(), Times.Never);
            Assert.False(_movieDataMock.testMovie.Watched);
        }

        [Fact]
        public void MarkMovieWatched_WithInvalidMovie_ShouldReturnNotFound()
        {
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _api.MarkeMovieWatched(1234, true);

            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public void MarkMovieWatched_WithDbError_ShouldReturnStatusInternalServerError()
        {
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() => throw new Exception());

            var result = _api.MarkeMovieWatched(1234, true);

            var codeResult = Assert.IsType<StatusCodeResult>(result);
            Assert.Equal(500, codeResult.StatusCode);
        }

        [Fact]
        public void MarkMovieWatched_WithMovieDeleted_ShouldReturnBadRequest()
        {
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            var result = _api.MarkeMovieWatched(1234, true);

            Assert.IsType<BadRequestResult>(result);
        }
    }
}
