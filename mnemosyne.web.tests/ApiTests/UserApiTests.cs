﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.web.Api;
using mnemosyne.web.tests.Mocks;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.ApiTests
{
    public class UserApiTests
    {
        private readonly UsersController _api;
        private readonly UserDataMock _userDataMock;

        public UserApiTests()
        {
            _userDataMock = new UserDataMock();
            _api = new UsersController(_userDataMock.Object);
        }

        #region VerifyEmail

        [Theory]
        [InlineData("tester@testland.com")]
        [InlineData(" tester@testland.com")]
        [InlineData("tester@testland.com ")]
        [InlineData(null)]
        public void VerifyEmail_WithEmail_ShouldReturnStatusOkWithTrue(string email)
        {
            _userDataMock
                .Setup(m => m.GetByEmail(It.IsAny<string>()))
                .Returns(() => null);

            var result = _api.VerifyEmail(email);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(true, okResult.Value);
        }

        [Theory]
        [InlineData("tester")]
        [InlineData("tester@")]
        [InlineData("tester@testland")]
        [InlineData("@testland")]
        [InlineData("@testland.com")]
        public void VerfyEmail_WithBadFormat_ShouldReturnStatusOkWithMessage(string email)
        {
            _userDataMock
                .Setup(m => m.GetByEmail(It.IsAny<string>()))
                .Returns(() => null);

            var result = _api.VerifyEmail(email);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Invalid email.", okResult.Value);
        }

        [Theory]
        [InlineData("")]
        public void VerifyEmail_WithEmptyEmail_ShouldReturnStatus400WithMessage(string email)
        {
            _userDataMock
                .Setup(m => m.GetByEmail(It.IsAny<string>()))
                .Returns(() => null);

            var result = _api.VerifyEmail(email);
            var status = Assert.IsType<ObjectResult>(result);

            Assert.Equal(400, status.StatusCode);
            Assert.Equal("Email not supplied.", status.Value);
        }

        #endregion

        #region VerifyUsername

        [Fact]
        public void VerifyUsername_WithUsername_ShouldReturnStatusOkWithTrue()
        {
            var result = _api.VerifyUsername("newusername");
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(true, okResult.Value);
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void VerifyUsername_WithNoUsername_ShouldReturnStatus400WithMessage(string username)
        {
            var result = _api.VerifyUsername(username);
            var status = Assert.IsType<ObjectResult>(result);
            Assert.Equal(400, status.StatusCode);
            Assert.Equal("Username not supplied.", status.Value);
        }

        [Theory]
        [InlineData("username")]
        [InlineData(" username")]
        [InlineData("username ")]
        public void VerifyUsername_WithNoTrimming_ShouldReturnStatusOkWithTrue(string username)
        {
            var result = _api.VerifyUsername(username);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(true, okResult.Value);
        }

        [Theory]
        [InlineData("user-name")]
        [InlineData("user_name")]
        [InlineData("us3rn4m3")]
        [InlineData("USERNAME")]
        public void VerifyUsername_WithSpecialFormat_ShouldReturnStatusOkWithTrue(string username)
        {
            var result = _api.VerifyUsername(username);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(true, okResult.Value);
        }

        [Theory]
        [InlineData("user name")]
        [InlineData("usern@me")]
        public void VerifyUsername_WithBadFormat_ShouldReturnStatusOkWithMessage(string username)
        {
            var result = _api.VerifyUsername(username);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Invalid username.", okResult.Value);
        }

        #endregion

        #region VerifyPassword

        [Theory]
        [InlineData("password")]
        [InlineData("passw0rd1")]
        [InlineData("pass@ord")]
        [InlineData("password!")]
        [InlineData("PassWord")]
        [InlineData("pa55@0rd!!")]
        [InlineData("12345678")]
        [InlineData("!@#$%^&*()_+-=\\|';:/.\",<>?")]
        [InlineData(null)]
        public void VerifyPassword_WithWalidPassword_ShouldReturnStatusOkWithTrue(string password)
        {
            var result = _api.VerifyPassword(password);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal(true, okResult.Value);
        }

        [Theory]
        [InlineData("12345")]
        [InlineData("")]
        public void VerifyPassword_WithIncorrectLength_ShouldReturnStatusOkWithMessage(string password)
        {
            var result = _api.VerifyPassword(password);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Password should be at least 6 characters long.", okResult.Value);
        }

        [Theory]
        [InlineData(" password")]
        [InlineData("pass word")]
        [InlineData("password ")]
        [InlineData(" ")]
        public void VerifyPassword_WithWhitespace_ShouldReturnStatusOkWithMessage(string password)
        {
            var result = _api.VerifyPassword(password);
            var okResult = Assert.IsType<OkObjectResult>(result);
            Assert.Equal("Whitespace is not allowed in passwords.", okResult.Value);
        }

        #endregion
    }
}
