﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using Xunit;

namespace mnemosyne.web.tests.UtilityTests
{
    public class PageModelUtilityTests
    {
        private enum TestEnum
        {
            [Display(Name = "Display Name")]
            WithDisplay = 0,
            WithoutDisplay = 1,
            [Display()]
            WithEmptyDisplay = 2
        }

        [Theory]
        [InlineData("Display Name", "WithDisplay")]
        [InlineData("WithoutDisplay", "WithoutDisplay")]
        [InlineData("WithEmptyDisplay", "WithEmptyDisplay")]
        public void GetEnumSelectList_ShouldReturnDisplayName(string displayName, string enumName)
        {
            var selectList = PageModelUtility.GetEnumSelectList<TestEnum>().ToList();
            Assert.True(selectList.Exists(s => s.Text == displayName && s.Value == enumName));
        }
    }
}
