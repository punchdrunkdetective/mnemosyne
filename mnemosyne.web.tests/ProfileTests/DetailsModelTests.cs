﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.muse;
using mnemosyne.web.Pages.Profiles;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.ProfileTests
{
    public class DetailsModelTests : PageModelTestBase
    {
        private readonly DetailsModel _detailsModel;

        public DetailsModelTests() : base()
        {
            _detailsModel = new DetailsModel(_userManagerMock.Object, _userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext),
                Details = null,
            };
        }

        [Fact]
        public void OnGet_WithValidUser_ShouldReadUser()
        {
            Assert.Null(_detailsModel.Details);
            _detailsModel.OnGet();
            _userManagerMock.Verify(m => m.GetUserId(), Times.Once);
            _userDataMock.Verify(m => m.GetById(_userManagerMock.actorId), Times.Once);
            Assert.Equal(_userManagerMock.actorId, _detailsModel.Details.Id);
            TestHelper.AssertEqual(_userDataMock.testUser, _detailsModel.Details);
        }
        
        [Fact]
        public void OnGet_WithValidUser_ShouldReturnPageResult()
        {
            var result = _detailsModel.OnGet();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithUserDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "User has been previously deleted.";
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            _detailsModel.OnGet();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidUser_ShouldRedirectToNotFoundPage()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _detailsModel.OnGet();

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullUser()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _detailsModel.OnGet();

            Assert.Null(_detailsModel.Details);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding user.";
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _detailsModel.OnGet();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _detailsModel.OnGet();

            Assert.IsType<PageResult>(result);
        }
    }
}
