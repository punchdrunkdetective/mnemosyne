﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.muse;
using mnemosyne.web.Pages.Users;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.UserTests
{
    public class DeleteModelTests : PageModelTestBase
    {
        private readonly DeleteModel _deleteModel;

        public DeleteModelTests()
        {
            _deleteModel = new DeleteModel(_userManagerMock.Object, _userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext),
                DeletedUser = null, // on get not called yet
            };
        }

        #region GET        

        [Fact]
        public void OnGet_WithValidUserId_ShouldReadUser()
        {
            _deleteModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetById(_userDataMock.testUserId), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldSetUser()
        {
            _deleteModel.OnGet(_userDataMock.testUserId);
            TestHelper.AssertEqual(_userDataMock.testUser, _deleteModel.DeletedUser);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldReturnPageResult()
        {
            var result = _deleteModel.OnGet(_userDataMock.testUserId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithValidUserId_ShouldSetDeletedByUser()
        {
            Assert.Null(_userDataMock.testUser.DeletedBy);
            _deleteModel.OnPost(_userDataMock.testUserId);
            Assert.Equal(_userManagerMock.actorId, _userDataMock.testUser.DeletedBy.DeletorUserid);
        }

        [Fact]
        public void OnPost_WithValidUserId_ShouldSetDeletedByDate()
        {
            Assert.Null(_userDataMock.testUser.DeletedBy);
            var now = DateTime.Now;
            _deleteModel.OnPost(_userDataMock.testUserId);
            TestHelper.AssertEqual(now, _userDataMock.testUser.DeletedBy.Date);
        }

        [Fact]
        public void OnGet_WithUserDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "User already deleted.";
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            _deleteModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidUserId_ShouldRedirectToNotFoundPage()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _deleteModel.OnGet(_userDataMock.testUserId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullUser()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _deleteModel.OnGet(_userDataMock.testUserId);
            Assert.Null(_deleteModel.DeletedUser);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movie.";
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _deleteModel.OnGet(_userDataMock.testUserId);
            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _deleteModel.OnGet(_userDataMock.testUserId);
            Assert.IsType<PageResult>(result);
        }

        #endregion

        #region POST

        [Fact]
        public void OnPost_WithValidUserId_ShouldDeleteUser()
        {
            _deleteModel.OnPost(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Once);
            _userDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidUserId_ShouldSetTempData()
        {
            _deleteModel.OnPost(_userDataMock.testUserId);
            Assert.Equal($"{_userDataMock.testUser.Username} Deleted", _tempData["Message"]);
        }

        [Fact]
        public void OnPost_WithValidUserId_ShouldRedirectToListPage()
        {
            var result = _deleteModel.OnPost(_userDataMock.testUserId);
            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./List", redirectResult.PageName);
        }

        [Fact]
        public void OnPost_WithInvalidUserId_ShouldNotDeleteUser()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _deleteModel.OnPost(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithInvalidUserId_ShouldNotSetDeletedByRecord()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _deleteModel.OnPost(_userDataMock.testUserId);
            Assert.Null(_userDataMock.testUser.DeletedBy);
        }

        [Fact]
        public void OnPost_WithInvalidUserId_ShouldReturnToNotFoundPage()
        {
            //TODO: consider just returning to page with an error message

            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _deleteModel.OnPost(_userDataMock.testUserId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnPost_WithDeleteError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error deleting user.";
            _userDataMock.Setup(m => m.Update(It.IsAny<User>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _deleteModel.OnPost(_userDataMock.testUserId);
            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithDeleteError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Throws<Exception>();

            var result = _deleteModel.OnPost(_userDataMock.testUserId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithActorId_ShouldNotDeleteUser()
        {
            _deleteModel.OnPost(_userManagerMock.actorId);
            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }
        
        [Fact]
        public void OnPost_WithActorId_ShouldSetErrorMessage()
        {
            const string errMsg = "Error: Cannot delete yourself.";
            _deleteModel.OnPost(_userManagerMock.actorId);
            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithActorId_ShouldReturnPageResult()
        {
            var result = _deleteModel.OnPost(_userManagerMock.actorId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithUserDeleted_ShouldNotDeleteUser()
        {
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            _deleteModel.OnPost(_userDataMock.testUserId);

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithUserDeleted_ShouldSetErrorMessage()
        {
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted
            const string errMsg = "User already deleted.";

            _deleteModel.OnPost(_userDataMock.testUserId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithUserDeleted_ShouldReturnPageResult()
        {
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            var result = _deleteModel.OnPost(_userDataMock.testUserId);

            Assert.IsType<PageResult>(result);
        }

        #endregion
    }
}
