﻿using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.web.Pages.Users;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace mnemosyne.web.tests.UserTests
{
    public class ListModelTests : PageModelTestBase
    {
        private readonly ListModel _listModel;

        public ListModelTests()
        {
            _listModel = new ListModel(_userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext)
            };
        }

        [Fact]
        public void OnGet_ShouldHaveAllUsers()
        {
            _listModel.OnGet();
            _userDataMock.Verify(m => m.GetAll(), Times.Once);
            TestHelper.AssertEqual(_userDataMock.testUserList.Where(u => u.DeletedBy == null), _listModel.Users);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding users.";
            _userDataMock
                .Setup(m => m.GetAll())
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _listModel.OnGet();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldHaveEmptyUserList()
        {
            _userDataMock
                .Setup(m => m.GetAll())
                .Throws<Exception>();

            _listModel.OnGet();

            Assert.Empty(_listModel.Users);
        }
    }
}
