﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.muse;
using mnemosyne.web.Pages.Users;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.UserTests
{
    public class DetailsModelTests : PageModelTestBase
    {
        private readonly DetailsModel _detailsModel;

        public DetailsModelTests() : base()
        {
            _detailsModel = new DetailsModel(_userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext),
                Details = null,
            };
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldReadUser()
        {
            _detailsModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetById(_userDataMock.testUserId), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldSetUser()
        {
            _detailsModel.OnGet(_userDataMock.testUserId);
            TestHelper.AssertEqual(_userDataMock.testUser, _detailsModel.Details);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldReturnPageResult()
        {
            var result = _detailsModel.OnGet(_userDataMock.testUserId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldReadEditedBy()
        {
            _detailsModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetEditedBy(_detailsModel.Details), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldReadCreatedBy()
        {
            _detailsModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetCreatedBy(_detailsModel.Details), Times.Once);
        }

        [Fact]
        public void OnGet_WithUserEdited_ShouldReadEditor()
        {
            var editedBy = new UserEditedBy() { EditorId = 777 };
            _userDataMock
                .Setup(m => m.GetEditedBy(_userDataMock.testUser))
                .Returns(() => editedBy);

            _detailsModel.OnGet(_userDataMock.testUserId);

            _userDataMock.Verify(m => m.GetById(editedBy.EditorId), Times.Once);
            Assert.NotNull(_detailsModel.Editor);
        }

        [Fact]
        public void OnGet_WithUserNotEdited_ShouldNotReadEditor()
        {
            _detailsModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetById(It.IsAny<int>()), Times.Once); // the original data read
            Assert.Null(_detailsModel.Editor);
        }

        [Fact]
        public void OnGet_WithUserCreated_ShouldReadCreator()
        {
            var createdBy = new UserCreatedBy() { CreatorId = 777 };
            _userDataMock
                .Setup(m => m.GetCreatedBy(_userDataMock.testUser))
                .Returns(() => createdBy);

            _detailsModel.OnGet(_userDataMock.testUserId);

            _userDataMock.Verify(m => m.GetById(createdBy.CreatorId), Times.Once);
            Assert.NotNull(_detailsModel.Creator);
        }

        [Fact]
        public void OnGet_WithUserNotCreated_ShouldNotReadCreator()
        {
            _detailsModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetById(It.IsAny<int>()), Times.Once); // the original data read
            Assert.Null(_detailsModel.Creator);
        }

        [Fact]
        public void OnGet_WithUserDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "User has been previously deleted.";
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            _detailsModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidUserId_ShouldRedirectToNotFoundPage()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _detailsModel.OnGet(_userDataMock.testUserId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullUser()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _detailsModel.OnGet(_userDataMock.testUserId);

            Assert.Null(_detailsModel.Details);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movie.";
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _detailsModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _detailsModel.OnGet(_userDataMock.testUserId);

            Assert.IsType<PageResult>(result);
        }
    }
}
