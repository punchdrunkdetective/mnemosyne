﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.muse;
using mnemosyne.web.Pages.Users;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.web.tests.UserTests
{
    public class EditModelTests : PageModelTestBase
    {
        private readonly EditModel _editModel;

        public EditModelTests() : base()
        {
            _editModel = new EditModel(_userManagerMock.Object, _passwordManagerMock.Object, _userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext),
                // on get not called yet
                Id = 0,
                Username = null,
                Password = null, 
                Role = null,
                GivenName = null,
                Surname = null,
                Email = null,
        };
        }

        #region GET

        [Fact]
        public void OnGet_WithValidUserId_ShouldReadUser()
        {
            _userDataMock.testUserId = 1;
            _editModel.OnGet(_userDataMock.testUserId);
            _userDataMock.Verify(m => m.GetById(_userDataMock.testUserId), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldSetUser()
        {
            _userDataMock.testUserId = 1;
            _editModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(_userDataMock.testUser.Id, _editModel.Id);
            Assert.Equal(_userDataMock.testUser.Username, _editModel.Username);
            Assert.Null(_editModel.Password);
            Assert.Equal(_userDataMock.testUser.Role, _editModel.Role);
            Assert.Equal(_userDataMock.testUser.GivenName, _editModel.GivenName);
            Assert.Equal(_userDataMock.testUser.Surname, _editModel.Surname);
            Assert.Equal(_userDataMock.testUser.Email, _editModel.Email);
        }

        [Fact]
        public void OnGet_WithValidUserId_ShouldReturnPageResult()
        {
            var result = _editModel.OnGet(_userDataMock.testUserId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithUserDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "User has been previously deleted.";
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            _editModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidUserId_ShouldRedirectToNotFoundPage()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _editModel.OnGet(_userDataMock.testUserId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullUser()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _editModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(0, _editModel.Id);
            Assert.Null(_editModel.Username);
            Assert.Null(_editModel.Password);
            Assert.Null(_editModel.Role);
            Assert.Null(_editModel.GivenName);
            Assert.Null(_editModel.Surname);
            Assert.Null(_editModel.Email);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movie.";
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _editModel.OnGet(_userDataMock.testUserId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _editModel.OnGet(_userDataMock.testUserId);

            Assert.IsType<PageResult>(result);
        }

        #endregion

        #region POST

        [Fact]
        public void OnPost_WithValidModelState_ShouldFindUser()
        {
            SetupPost();
            _editModel.OnPost();
            _userDataMock.Verify(m => m.GetById(_userDataMock.testUserId), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldUpdateUser()
        {
            User savedUser = null;
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    savedUser = u;
                    return u;
                });

            SetupPost();
            _editModel.OnPost();

            Assert.Equal(_editModel.Id, savedUser.Id);
            Assert.Equal(_editModel.Username, savedUser.Username);
            Assert.Equal(_editModel.Password, savedUser.Password);
            Assert.Equal(_editModel.Role, savedUser.Role);
            Assert.Equal(_editModel.GivenName, savedUser.GivenName);
            Assert.Equal(_editModel.Surname, savedUser.Surname);
            Assert.Equal(_editModel.Email, savedUser.Email);

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Once);
            _userDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldEncodePassword()
        {
            SetupPost();
            _editModel.OnPost();
            _passwordManagerMock.Verify(m => m.EncodePassword(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetTempMessage()
        {
            SetupPost();
            _editModel.OnPost();
            Assert.Equal("User Saved", _tempData["Message"]);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldRedirectToDetailsPage()
        {
            SetupPost();
            var result = _editModel.OnPost();

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./Details", redirectResult.PageName);

            IDictionary<string, object> routeValues = redirectResult.RouteValues;
            Assert.Single(routeValues);

            var userId = Assert.Contains("userId", routeValues);
            Assert.Equal(_editModel.Id, userId);
        }

        [Fact]
        public void OnPost_WithNoUsernameChange_ShouldPassValidation()
        {
            string testName = "unchanged username";
            _userDataMock.testUser.Username = testName;
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns<string>(username => username == testName);

            SetupPost();
            _editModel.Username = testName;
            _editModel.OnPost();

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Once);
            _userDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithNoEmailChange_ShouldPassValidation()
        {
            string testEmail = "unchanged email";
            _userDataMock.testUser.Email = testEmail;
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns<string>(email => email == testEmail);

            SetupPost();
            _editModel.Email = testEmail;
            _editModel.OnPost();

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Once);
            _userDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void OnPost_WithNoPasswordChange_ShouldNotUpdatePassword(string password)
        {
            User savedUser = null;
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    savedUser = u;
                    return u;
                });

            SetupPost();
            _editModel.Password = password;
            _editModel.OnPost();

            Assert.NotEqual(password, savedUser.Password); 
            Assert.Equal(_userDataMock.testUser.Password, savedUser.Password);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetEditedByUser()
        {
            UserEditedBy editData = null;
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    editData = u.EditedBy;
                    return u;
                });

            SetupPost();
            _editModel.OnPost();
            Assert.Equal(_userManagerMock.actorId, editData.EditorId);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetEditedByDate()
        {
            var now = DateTime.Now;
            UserEditedBy editData = null;
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    editData = u.EditedBy;
                    return u;
                });

            SetupPost();
            _editModel.OnPost();
            TestHelper.AssertEqual(now, editData.Date);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotUpdateMovie()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            SetupPost();
            _editModel.OnPost();
            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldReturnPageResult()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            SetupPost();
            var result = _editModel.OnPost();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithNonUniqueUsername_ShouldNotEditUser()
        {
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns(true);

            SetupPost();
            _editModel.OnPost();

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithNonUniqueUsername_ShouldSetErrorMessage()
        {
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns(true);

            SetupPost();
            _editModel.OnPost();

            Assert.Equal("Username already registered.", _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithNonUniqueUsername_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns(true);

            SetupPost();
            var result = _editModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithNonUniqueEmail_ShouldNotEditUser()
        {
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns(true);

            SetupPost();
            _editModel.OnPost();

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithNonUniqueEmail_ShouldSetErrorMessage()
        {
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns(true);

            SetupPost();          
            _editModel.OnPost();

            Assert.Equal("Email already registered.", _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithNonUniqueEmail_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns(true);

            SetupPost();            
            var result = _editModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithUpdateError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error updating user.";
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            SetupPost();
            _editModel.OnPost();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithUpdateError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Throws<Exception>();

            SetupPost();
            var result = _editModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithNoExistingEditedBy_ShouldCreateEditedBy()
        {
            _userDataMock
                .Setup(m => m.GetEditedBy(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    u.EditedBy = null;
                    return null;
                });

            UserEditedBy editData = null;
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    editData = u.EditedBy;
                    return u;
                });

            SetupPost();
            _editModel.OnPost();
            _userDataMock.Verify(m => m.GetEditedBy(It.IsAny<User>()), Times.Once);
            Assert.NotNull(editData);
        }

        [Fact]
        public void OnPost_WithExistingEditedBy_ShouldNotCreateEditedBy()
        {
            var previousEdit = new UserEditedBy()
            {
                EditorId = _userManagerMock.actorId,
                Date = DateTime.Now,
            };

            _userDataMock
                .Setup(m => m.GetEditedBy(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    u.EditedBy = previousEdit;
                    return previousEdit;
                });

            UserEditedBy editData = null;
            _userDataMock
                .Setup(m => m.Update(It.IsAny<User>()))
                .Returns<User>(u =>
                {
                    editData = u.EditedBy;
                    return u;
                });

            SetupPost();
            _editModel.OnPost();
            _userDataMock.Verify(m => m.GetEditedBy(It.IsAny<User>()), Times.Once);
            Assert.Same(previousEdit, editData);
        }

        [Fact]
        public void OnPost_WithUserDeleted_ShouldNotUpdateUser()
        {
            SetupPost();
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            _editModel.OnPost();

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithUserDeleted_ShouldSetErrorMessage()
        {
            SetupPost();
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted
            const string errMsg = "Can not update deleted user.";

            _editModel.OnPost();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithUserDeleted_ShouldReturnPageResult()
        {
            SetupPost();
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); //if DeletedBy exists, then User has been deleted

            var result = _editModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        #endregion

        private void SetupPost()
        {
            // get happened
            _editModel.Id = _userDataMock.testUserId; 
            _editModel.Username = "edited username";
            _editModel.Password = "new password";
            _editModel.Role = "edited role";
            _editModel.Surname = "edited first name";
            _editModel.GivenName = "edited last name";
            _editModel.Email = "edited  email";            
        }
    }
}
