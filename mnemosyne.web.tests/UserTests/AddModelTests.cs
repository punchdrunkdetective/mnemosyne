﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.muse;
using mnemosyne.web.Pages.Users;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.web.tests.UserTests
{
    public class AddModelTests : PageModelTestBase
    {
        private readonly AddModel _addModel;

        public AddModelTests()
        {
            _addModel = new AddModel(_userManagerMock.Object, _passwordManagerMock.Object, _userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext),
                UserModel = new User()
                {
                    //represents user input, make sure it doesn't equal _testUser from base
                    Username = "new username",
                    Password = "new password",
                    Role = "", // can be "Admin" or ""
                    Surname = "new First Name",
                    GivenName = "new Last Name",
                    Email = null,
                }
            };
        }

        [Fact]
        public void OnGet_ShouldReturnPageResult()
        {
            _addModel.UserModel = null; //reset from constructor setup
            var result = _addModel.OnGet();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldCreateUser()
        {
            _addModel.OnPost();
            _userDataMock.Verify(m => m.Create(_addModel.UserModel), Times.Once);
            _userDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldEncodePassword()
        {
            _addModel.OnPost();
            _passwordManagerMock.Verify(m => m.EncodePassword(It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldRedirectToDetailsPage()
        {
            var result = _addModel.OnPost();

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./Details", redirectResult.PageName);

            IDictionary<string, object> routeValues = redirectResult.RouteValues;
            Assert.Single(routeValues);

            var movieId = Assert.Contains("userId", routeValues);
            Assert.Equal(_addModel.UserModel.Id, movieId);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldSetTempMessage()
        {
            _addModel.OnPost();
            Assert.Equal("User Created", _tempData["Message"]);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldSetCreatedByDate()
        {
            var now = DateTime.Now;
            _addModel.OnPost();
            TestHelper.AssertEqual(now, _addModel.UserModel.CreatedBy.Date);
        }

        [Fact]
        public void OnPost_WithValidModelStates_ShouldSetCreatedByUser()
        {
            _addModel.OnPost();
            Assert.Equal(_userManagerMock.actorId, _addModel.UserModel.CreatedBy.CreatorId);
        }

        [Fact]
        public void OnPost_WithInvalidModelStates_ShouldNotCreateUser()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            _addModel.OnPost();
            _userDataMock.Verify(m => m.Create(_addModel.UserModel), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithInvalidModelStates_ShouldReturnPageResult()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            var result = _addModel.OnPost();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithNonUniqueUsername_ShouldNotCreateUser()
        {
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns(true);

            _addModel.OnPost();

            _userDataMock.Verify(m => m.Create(_addModel.UserModel), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithNonUniqueUsername_ShouldSetErrorMessage()
        {
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns(true);

            _addModel.OnPost();

            Assert.Equal("Username already registered.", _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithNonUniqueUsername_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.UsernameExists(It.IsAny<string>()))
                .Returns(true);

            var result = _addModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithNonUniqueEmail_ShouldNotCreateUser()
        {
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns(true);

            _addModel.OnPost();

            _userDataMock.Verify(m => m.Create(_addModel.UserModel), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithNonUniqueEmail_ShouldSetErrorMessage()
        {
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns(true);

            _addModel.OnPost();

            Assert.Equal("Email already registered.", _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithNonUniqueEmail_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.EmailExists(It.IsAny<string>()))
                .Returns(true);

            var result = _addModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithCreateError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error creating user.";
            _userDataMock
                .Setup(m => m.Create(It.IsAny<User>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _addModel.OnPost();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithCreateError_ShouldReturnPageResult()
        {
            _userDataMock
                .Setup(m => m.Create(It.IsAny<User>()))
                .Throws<Exception>();

            var result = _addModel.OnPost();

            Assert.IsType<PageResult>(result);
        }
    }
}
