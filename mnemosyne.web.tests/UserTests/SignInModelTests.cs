﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Routing;
using mnemosyne.muse;
using mnemosyne.web.Pages.Users;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace mnemosyne.web.tests.UserTests
{
    public class SignInModelTests : PageModelTestBase
    {
        private readonly SignInModel _signInModel;

        public SignInModelTests() : base()
        {
            // all sign in tests assume unauthenticated actor
            _userManagerMock.isAuthenticated = false;
            _userManagerMock.actorId = -1;
            _userManagerMock.actor = null;

            _signInModel = new SignInModel(_userManagerMock.Object, _userDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Url = new UrlHelper(_actionContext)
            };
        }

        #region OnGet

        [Theory]
        [InlineData(null)]
        [InlineData("./someplace")]
        public void OnGet_WithAuthenticatedUser_ShouldRedirectToUrl(string redirectUrl)
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok

            IActionResult result = redirectUrl == null
                ? _signInModel.OnGet()
                : _signInModel.OnGet(redirectUrl);

            var redirectResult = Assert.IsType<LocalRedirectResult>(result);
            if (redirectUrl == null)
            {
                Assert.Equal("/", redirectResult.Url);
            }
            else
            {
                Assert.Equal(redirectUrl, redirectResult.Url);
            }
        }

        [Theory]
        [InlineData(null)]
        [InlineData("./someplace")]
        public void OnGet_ShouldReturnPage(string redirectUrl)
        {
            IActionResult result = redirectUrl == null
                ? _signInModel.OnGet()
                : _signInModel.OnGet(redirectUrl);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithError_ShouldReturnPage()
        {
            _userManagerMock
                .Setup(m => m.IsAuthenticated())
                .Throws<Exception>();

            var result = _signInModel.OnGet();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithError_ShouldSetErrorMessage()
        {
            const string errorMsg = "Can not authenticate.";
            _userManagerMock
                .Setup(m => m.IsAuthenticated())
                .Callback(() => throw new Exception(errorMsg));

            _signInModel.OnGet();

            Assert.Equal(errorMsg, _viewData["ErrorMessage"]);
        }

        #endregion

        #region OnPost

        [Fact]
        public async Task OnPost_WithValidCredentials_ShouldAuthenticateUser()
        {
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            await _signInModel.OnPostAsync();

            _userDataMock.Verify(m => m.GetByUsernameAndPassword(username, password), Times.Once);
            _userManagerMock.Verify(m => m.SignIn(It.IsAny<ClaimsPrincipal>()), Times.Once);
        }

        [Fact]
        public async Task OnPost_WithValidCredentials_ShouldSetClaims()
        {
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            await _signInModel.OnPostAsync();

            Assert.True(_userManagerMock.actor.HasClaim(ClaimTypes.NameIdentifier, _userDataMock.testUser.Id.ToString()));
            Assert.True(_userManagerMock.actor.HasClaim(ClaimTypes.Role, _userDataMock.testUser.Role));
            Assert.True(_userManagerMock.actor.HasClaim(ClaimTypes.Name, _userDataMock.testUser.InformalName));
        }

        [Fact]
        public async Task OnPost_WithValidCredentials_ShouldSetLastLogin()
        {
            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            await _signInModel.OnPostAsync();
            Assert.NotNull(_signInModel.UserForm.LastLogin);
            TestHelper.AssertEqual(DateTime.Now, _signInModel.UserForm.LastLogin.Value);
            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Once);
            _userDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public async Task OnPost_WithValidCredentials_ShouldSetTempMessage()
        {
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            await _signInModel.OnPostAsync();

            Assert.NotNull(_tempData["SignedIn"]);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("./someplace")]
        public async Task OnPost_WithValidCredentials_ShouldRedirect(string redirectUrl)
        {
            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";

            var result = redirectUrl == null
                ? await _signInModel.OnPostAsync()
                : await _signInModel.OnPostAsync(redirectUrl);

            var redirectResult = Assert.IsType<LocalRedirectResult>(result);
            if (redirectUrl == null)
            {
                Assert.Equal("/", redirectResult.Url);
            }
            else
            {
                Assert.Equal(redirectUrl, redirectResult.Url);
            }
        }

        [Fact]
        public async Task OnPost_WithAuthenticatedUser_ShouldNotAuthenticateUser()
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            await _signInModel.OnPostAsync();

            _userManagerMock.Verify(m => m.SignIn(It.IsAny<ClaimsPrincipal>()), Times.Never);
        }

        [Fact]
        public async Task OnPost_WithAuthenticatedUser_ShouldNotSetLastLogin()
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            _signInModel.UserForm.LastLogin = DateTime.Now.AddDays(-10);

            await _signInModel.OnPostAsync();

            TestHelper.AssertNotEqual(DateTime.Now, _signInModel.UserForm.LastLogin.Value);
            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public async Task OnPost_WithAuthenticatedUser_ShouldNotSetTempMessage()
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            await _signInModel.OnPostAsync();

            Assert.Null(_tempData["SignedIn"]);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("./someplace")]
        public async Task OnPost_WithAuthenticatedUser_ShouldRedirectToUrl(string redirectUrl)
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            var result = redirectUrl == null
                ? await _signInModel.OnPostAsync()
                : await _signInModel.OnPostAsync(redirectUrl);

            var redirectResult = Assert.IsType<LocalRedirectResult>(result);
            if (redirectUrl == null)
            {
                Assert.Equal("/", redirectResult.Url);
            }
            else
            {
                Assert.Equal(redirectUrl, redirectResult.Url);
            }
        }

        [Fact]
        public async Task OnPost_WithDeletedUser_ShouldNotAuthenticateUser()
        {
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            _userDataMock.testUser.DeletedBy = new UserDeletedBy(); 

            await _signInModel.OnPostAsync();

            _userManagerMock.Verify(m => m.SignIn(It.IsAny<ClaimsPrincipal>()), Times.Never);
        }

        [Fact]
        public async Task OnPost_WithDeletedUser_ShouldNotUpdateUserRecord()
        {
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            _userDataMock.testUser.DeletedBy = new UserDeletedBy();

            await _signInModel.OnPostAsync();

            _userDataMock.Verify(m => m.Update(It.IsAny<User>()), Times.Never);
            _userDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public async Task OnPost_WithDeletedUser_ShouldSetErrorMessage()
        {
            const string errMsg = "User has been deleted.";
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            _userDataMock.testUser.DeletedBy = new UserDeletedBy();

            await _signInModel.OnPostAsync();
            
            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public async Task OnPost_WithDeletedUser_ShouldReturnPage()
        {            
            const string username = "username";
            const string password = "password";

            _signInModel.UserForm.Username = username;
            _signInModel.UserForm.Password = password;
            _userDataMock.testUser.DeletedBy = new UserDeletedBy();

            var result = await _signInModel.OnPostAsync();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public async Task OnPost_WithInvalidCredentials_ShouldNotAuthenticateUser()
        {
            _userDataMock
                .Setup(m => m.GetByUsernameAndPassword(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(() => null);

            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            var result = await _signInModel.OnPostAsync();

            _userManagerMock.Verify(m => m.SignIn(It.IsAny<ClaimsPrincipal>()), Times.Never);
        }

        [Fact]
        public async Task OnPost_WithInvalidCredentials_ShouldNotSetClaims()
        {
            _userDataMock
                .Setup(m => m.GetByUsernameAndPassword(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(() => null);

            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            await _signInModel.OnPostAsync();

            Assert.Null(_userManagerMock.actor); // claim principle never created
        }

        [Fact]
        public async Task OnPost_WithInvalidCredentials_ShouldReturnUnauthorized()
        {
            _userDataMock
                .Setup(m => m.GetByUsernameAndPassword(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(() => null);

            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            var result = await _signInModel.OnPostAsync();

            var unauthorizedResult = Assert.IsType<UnauthorizedResult>(result);
            Assert.Equal(401, unauthorizedResult.StatusCode);
        }

        [Fact]
        public async Task OnPost_WithSignInError_ShouldReturnPage()
        {
            _userManagerMock
                .Setup(m => m.SignIn(It.IsAny<ClaimsPrincipal>()))
                .Throws<Exception>();

            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            var result = await _signInModel.OnPostAsync();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public async Task OnPost_WithSignInError_ShouldSetErrorMessage()
        {
            const string errMsg = "Cannot sign in";
            _userManagerMock
                .Setup(m => m.SignIn(It.IsAny<ClaimsPrincipal>()))
                .Callback(() => throw new Exception(errMsg));

            _signInModel.UserForm.Username = "username";
            _signInModel.UserForm.Password = "password";
            await _signInModel.OnPostAsync();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public async Task OnPost_WithInvalidModelState_ShouldNotAuthenticateUser()
        {
            _modelState.AddModelError("Error Key", "Error Message");

            _signInModel.UserForm.Username = "invalid username";
            _signInModel.UserForm.Password = "invalid password";
            await _signInModel.OnPostAsync();

            _userDataMock.Verify(m => m.GetByUsernameAndPassword(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
            _userManagerMock.Verify(m => m.SignIn(It.IsAny<ClaimsPrincipal>()), Times.Never);
        }

        [Fact]
        public async Task OnPost_WithInvalidModelState_ShouldNotSetClaims()
        {
            _modelState.AddModelError("Error Key", "Error Message");

            _signInModel.UserForm.Username = "invalid username";
            _signInModel.UserForm.Password = "invalid password";
            await _signInModel.OnPostAsync();

            Assert.Null(_userManagerMock.actor); // claim principle never created
        }

        [Fact]
        public async Task OnPost_WithInvalidModelState_ShouldReturnPageResult()
        {
            _modelState.AddModelError("Error Key", "Error Message");

            _signInModel.UserForm.Username = "invalid username";
            _signInModel.UserForm.Password = "invalid password";
            var result = await _signInModel.OnPostAsync();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public async Task OnPost_WithInvalidModelState_ShouldNotSetErrorMessage()
        {
            _modelState.AddModelError("Error Key", "Error Message");

            _signInModel.UserForm.Username = "invalid username";
            _signInModel.UserForm.Password = "invalid password";
            await _signInModel.OnPostAsync();

            Assert.Null(_viewData["ErrorMessage"]);
        }

        #endregion

        #region OnPostSignOut

        [Fact]
        public async Task OnPostSignOut_WithAuthenticatedUser_ShouldSignOutUser()
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            await _signInModel.OnPostSignOutAsync();
            _userManagerMock.Verify(m => m.SignOut(), Times.Once);
        }

        [Fact]
        public async Task OnPostSignOut_WithAuthenticatedUser_ShouldRedirectToRoot()
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            var result = await _signInModel.OnPostSignOutAsync();
            var redirectResult = Assert.IsType<LocalRedirectResult>(result);
            Assert.Equal("/", redirectResult.Url);
        }

        [Fact]
        public async Task OnPostSignOut_WithUnauthenticatedUser_ShouldNotSignOutUser()
        {
            await _signInModel.OnPostSignOutAsync();
            _userManagerMock.Verify(m => m.SignOut(), Times.Never);
        }

        [Fact]
        public async Task OnPostSignOut_WithUnauthenticatedUser_ShouldRedirectToRoot()
        {
            var result = await _signInModel.OnPostSignOutAsync();
            var redirectResult = Assert.IsType<LocalRedirectResult>(result);
            Assert.Equal("/", redirectResult.Url);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("./someplace")]
        public async Task OnPostSignOut_WithError_ShouldRedirectToUrl(string errorUrl)
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            _userManagerMock
                .Setup(m => m.SignOut())
                .Throws<Exception>();

            var result = errorUrl == null
                ? await _signInModel.OnPostSignOutAsync()
                : await _signInModel.OnPostSignOutAsync(errorUrl);

            var redirectResult = Assert.IsType<LocalRedirectResult>(result);
            if (errorUrl == null)
            {
                Assert.Equal("/", redirectResult.Url);
            }
            else
            {
                Assert.Equal(errorUrl, redirectResult.Url);
            }
        }

        [Fact]
        public async Task OnPostSignOut_WithError_ShouldSetTempMessage()
        {
            _userManagerMock.isAuthenticated = true; // actor id and actor not being set should be ok
            const string errMsg = "Cannot sign out.";
            _userManagerMock
                .Setup(m => m.SignOut())
                .Callback(() => throw new Exception(errMsg));

            var result = await _signInModel.OnPostSignOutAsync();

            Assert.True(_tempData.ContainsKey("Message"));
            Assert.Equal(errMsg, _tempData["Message"]);
        }

        #endregion
    }
}
