﻿using System;
using Xunit;

namespace mnemosyne.web.tests.Helpers
{
    static public partial class TestHelper
    {
        static public void AssertEqual(DateTime? expectedDate, DateTime? actualDate)
        {
            Assert.Equal(expectedDate.HasValue, actualDate.HasValue);
            if (expectedDate.HasValue)
            {
                AssertEqual(expectedDate.Value, actualDate.Value);
            }
        }

        static public void AssertEqual(DateTime expectedDate, DateTime actualDate)
        {
            expectedDate = new DateTime(expectedDate.Year, expectedDate.Month, expectedDate.Day, expectedDate.Hour, expectedDate.Minute, 0);
            actualDate = new DateTime(actualDate.Year, actualDate.Month, actualDate.Day, actualDate.Hour, actualDate.Minute, 0);
            Assert.Equal(expectedDate, actualDate);
        }

        static public void AssertNotEqual(DateTime expectedDate, DateTime actualDate)
        {
            expectedDate = new DateTime(expectedDate.Year, expectedDate.Month, expectedDate.Day, expectedDate.Hour, expectedDate.Minute, 0);
            actualDate = new DateTime(actualDate.Year, actualDate.Month, actualDate.Day, actualDate.Hour, actualDate.Minute, 0);
            Assert.NotEqual(expectedDate, actualDate);
        }
    }
}
