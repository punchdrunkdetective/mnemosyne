﻿using mnemosyne.muse;
using Xunit;

namespace mnemosyne.web.tests.Helpers
{
    static public partial class TestHelper
    {
        static public void AssertEqual(Rating expected, Rating actual)
        {
            Assert.Equal(expected.MovieId, actual.MovieId);
            Assert.Equal(expected.UserId, actual.UserId);
            Assert.Equal(expected.Value, actual.Value);
        }
    }
}
