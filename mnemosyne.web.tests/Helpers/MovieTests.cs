﻿using mnemosyne.muse;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace mnemosyne.web.tests.Helpers
{
    static public partial class TestHelper
    {
        static public void AssertEqual(Movie expected, Movie actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Title, actual.Title);
            Assert.Equal(expected.Director, actual.Director);
            Assert.Equal(expected.ReleaseYear, actual.ReleaseYear);
            Assert.Equal(expected.Genre, actual.Genre);
        }

        /// <summary>
        /// Asserts two collections are equal by checking count and order. order passed in will be used.
        /// </summary>
        static public void AssertEqual<T>(IEnumerable<Movie> expected, IEnumerable<Movie> actual, Func<Movie, T> order)
        {
            AssertEqual(expected.OrderBy(order), actual.OrderBy(order));
        }

        /// <summary>
        /// Asserts two collections are equal by checking count and order. order will be whatever order 
        /// collections are already in.
        /// </summary>
        static public void AssertEqual(IEnumerable<Movie> expected, IEnumerable<Movie> actual)
        {
            var expectedList = expected.ToList();
            var actualList = actual.ToList();

            Assert.Equal(expectedList.Count, actualList.Count);
            for (var i = 0; i < actualList.Count; i++)
            {
                AssertEqual(expectedList[i], actualList[i]);
            }
        }
    }
}
