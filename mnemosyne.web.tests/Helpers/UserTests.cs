﻿using mnemosyne.muse;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace mnemosyne.web.tests.Helpers
{
    static public partial class TestHelper
    {
        static public void AssertEqual(User expected, User actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Username, actual.Username);
            Assert.Equal(expected.Password, actual.Password);
            Assert.Equal(expected.Role, actual.Role);
            Assert.Equal(expected.Email, actual.Email);
            Assert.Equal(expected.GivenName, actual.GivenName);
            Assert.Equal(expected.Surname, actual.Surname);
            Assert.Equal(expected.InformalName, actual.InformalName);
            AssertEqual(expected.LastLogin, actual.LastLogin);
        }

        /// <summary>
        /// Asserts two collections are equal by checking count and order. order will be whatever order 
        /// collections are already in.
        /// </summary>
        static public void AssertEqual(IEnumerable<User> expected, IEnumerable<User> actual)
        {
            var expectedList = expected.ToList();
            var actualList = actual.ToList();

            Assert.Equal(expectedList.Count, actualList.Count);
            for (var i = 0; i < actualList.Count; i++)
            {
                AssertEqual(expectedList[i], actualList[i]);
            }
        }
    }
}
