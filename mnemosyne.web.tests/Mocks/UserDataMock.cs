﻿using mnemosyne.data;
using mnemosyne.muse;
using Moq;
using System.Collections.Generic;

namespace mnemosyne.web.tests.Mocks
{
    public class UserDataMock : Mock<IUserData>
    {
        public int testUserId;
        public User testUser;
        public List<User> testUserList;

        public UserDataMock()
        {
            testUserId = 4321;
            testUser = new User
            {
                Id = testUserId,
                Username = "username",
                Password = "password",
                Role = "Tester",
                GivenName = "First Name",
                Surname = "Last Name",
                Email = "tester@testland.com",
                CreatedBy = null,
                EditedBy = null,
                DeletedBy = null,
            };

            testUserList = new List<User>() // order does not matter
            {
                new User()
                {
                    Id = 1,
                    Username = "user1",
                    Password = "password1",
                    Role = "Admin",
                    GivenName = "FirstName1",
                    Surname = "LastName1",
                },
                new User()
                {
                    Id = 2,
                    Username = "user2",
                    Password = "password2",
                    Role = "",
                    GivenName = "FirstName2",
                    Surname = "LastName2",
                },
                new User()
                {
                    Id = 3,
                    Username = "user3",
                    Password = "password3",
                    Role = "Admin",
                    GivenName = "FirstName3",
                    Surname = "LastName3",
                },
                new User()
                {
                    Id = 4,
                    Username = "user4",
                    Password = "password4",
                    Role = "",
                    GivenName = null,
                    Surname = null,
                },
                new User()
                {
                    Id = 5,
                    Username = "user5",
                    Password = "password5",
                    Role = "Admin",
                    GivenName = null,
                    Surname = null,
                },
                new User()
                {
                    Id = 6,
                    Username = "user6",
                    Password = "password6",
                    Role = "",
                    GivenName = null,
                    Surname = null,
                    DeletedBy = new UserDeletedBy(),
                }
            };

            Setup(m => m.GetById(It.IsAny<int>()))
            .Returns<int>((id) =>
            {
                // always get a valid user by default
                testUserId = id;
                testUser.Id = id;
                return testUser;
            });

            Setup(m => m.GetByUsernameAndPassword(It.IsAny<string>(), It.IsAny<string>()))
            .Returns<string, string>((username, password) =>
            {
                testUser.Username = username;
                testUser.Password = password;
                return testUser;
            });

            Setup(m => m.GetByEmail(It.IsAny<string>()))
            .Returns<string>((email) =>
            {
                testUser.Email = email;
                return testUser;
            });

            Setup(m => m.UsernameExists(It.IsAny<string>()))
            .Returns(false);

            Setup(m => m.EmailExists(It.IsAny<string>()))
            .Returns(false);

            Setup(m => m.GetAll())
            .Returns(() => testUserList);

            Setup(m => m.Create(It.IsAny<User>()))
            .Returns<User>(u =>
            {
                testUser = u;
                return u;
            });

            Setup(m => m.Update(It.IsAny<User>()))
            .Returns<User>(u => u); //TODO: should this be the same implementation of the mocked create method?

            Setup(m => m.DeleteById(It.IsAny<int>()))
            .Returns<int>(id =>
            {
                // always get a valid user by default
                testUserId = id;
                testUser.Id = id;
                return testUser;
            });

            Setup(m => m.GetCreatedBy(It.IsAny<User>()))
            .Returns<User>(u => u.CreatedBy);

            Setup(m => m.GetEditedBy(It.IsAny<User>()))
            .Returns<User>(u => u.EditedBy);

            Setup(m => m.GetDeletedBy(It.IsAny<User>()))
            .Returns<User>(u => u.DeletedBy);
        }
    }
}
