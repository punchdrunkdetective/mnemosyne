﻿using mnemosyne.muse;
using Moq;

namespace mnemosyne.web.tests.Mocks
{
    public class PasswordManagerMock : Mock<IPasswordManager>
    {
        public PasswordManagerMock()
        {
            Setup(m => m.EncodePassword(It.IsAny<string>()))
            .Returns<string>((pw) => pw);
        }
    }
}
