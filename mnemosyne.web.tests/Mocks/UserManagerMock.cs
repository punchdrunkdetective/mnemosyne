﻿using Microsoft.AspNetCore.Authentication.Cookies;
using mnemosyne.web.Authentication;
using Moq;
using System.Collections.Generic;
using System.Security.Claims;

namespace mnemosyne.web.tests.Mocks
{
    public class UserManagerMock : Mock<IUserManager>
    {

        public bool isAuthenticated;
        public int actorId;
        public ClaimsPrincipal actor;

        public UserManagerMock()
        {
            // all tests default to a authenticated admin actor
            isAuthenticated = true;
            actorId = 1234;
            actor = new ClaimsPrincipal(new ClaimsIdentity(
                new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, actorId.ToString()),
                    new Claim(ClaimTypes.Role, "Admin"), // role checking is done by attributes, so this shouldn't matter 
                },
                CookieAuthenticationDefaults.AuthenticationScheme));

            // user manager only deals with the "actor" or signed in user, not the user under test

            Setup(m => m.IsAuthenticated())
            .Returns(() => isAuthenticated);

            Setup(m => m.SignIn(It.IsAny<ClaimsPrincipal>()))
            .Callback<ClaimsPrincipal>(p =>
            {
                actorId = int.Parse(p.FindFirst(ClaimTypes.NameIdentifier).Value);
                actor = p;
                isAuthenticated = true;
            });

            Setup(m => m.SignOut())
            .Callback(() =>
            {
                actorId = -1;
                actor = null;
                isAuthenticated = false;
            });

            Setup(m => m.GetUserId())
            .Returns(() => actorId);

            Setup(m => m.GetUser())
            .Returns(() => actor);
        }
    }
}
