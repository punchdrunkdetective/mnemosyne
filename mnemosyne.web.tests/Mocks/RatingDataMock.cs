﻿using mnemosyne.data;
using mnemosyne.muse;
using Moq;

namespace mnemosyne.web.tests.Mocks
{
    public class RatingDataMock : Mock<IRatingData>
    {
        public Rating testRating = new Rating()
        {
            MovieId = 0,
            UserId = 0,
            Value = 3,
        };

        public RatingDataMock()
        {
            Setup(mock => mock.GetById(It.IsAny<int>(), It.IsAny<int>()))
            .Returns<int, int>((movieId, userId) =>
            {
                testRating.MovieId = movieId;
                testRating.UserId = userId;
                return testRating;
            });

            Setup(mock => mock.Create(It.IsAny<Rating>()))
            .Returns<Rating>(r =>
            {
                testRating = r;
                return testRating;
            });

            Setup(mock => mock.Update(It.IsAny<Rating>()))
            .Returns<Rating>(r =>
            {
                testRating = r;
                return testRating;
            });
        }
    }
}
