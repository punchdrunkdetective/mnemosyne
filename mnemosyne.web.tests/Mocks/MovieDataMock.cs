﻿using mnemosyne.data;
using mnemosyne.muse;
using Moq;
using System.Collections.Generic;

namespace mnemosyne.web.tests.Mocks
{
    public class MovieDataMock : Mock<IMovieData>
    {
        public Movie testMovie;
        public List<Movie> testMovieList;

        public MovieDataMock()
        {
            testMovie = new Movie()
            {
                Id = 0, //TODO: set this to 1 by default?
                Title = "Test Movie",
                Director = "Test Director",
                ReleaseYear = 2021,
                Genre = Genre.Experimental,
                Poster = "https://m.media-amazon.com/images/M/MV5BYjEzN2FlYmYtNDkwMC00NGFkLWE5ODctYmE5NmYxNzE2MmRiXkEyXkFqcGdeQXVyMjMwODc5Mw@@._V1_SX300.jpg",
                Watched = true,
                CreatedBy = null,
                EditedBy = null,
                DeletedBy = null,
            };

            testMovieList = new List<Movie> { //make sure title order is random 
                new Movie()
                {
                    Id = 1,
                    Title = "B",
                    Director = "Test Director 1",
                    ReleaseYear = 2021,
                    Genre = Genre.Experimental,
                    Poster = "https://m.media-amazon.com/images/M/MV5BYjEzN2FlYmYtNDkwMC00NGFkLWE5ODctYmE5NmYxNzE2MmRiXkEyXkFqcGdeQXVyMjMwODc5Mw@@._V1_SX300.jpg",
                },
                new Movie()
                {
                    Id = 2,
                    Title = "W",
                    Director = "Test Director 2",
                    ReleaseYear = 2021,
                    Genre = Genre.Experimental,
                    Poster = "",
                },
                new Movie()
                {
                    Id = 3,
                    Title = "R",
                    Director = "Test Director 3",
                    ReleaseYear = 2021,
                    Genre = Genre.Experimental,
                    Poster = "https://m.media-amazon.com/images/M/MV5BYjEzN2FlYmYtNDkwMC00NGFkLWE5ODctYmE5NmYxNzE2MmRiXkEyXkFqcGdeQXVyMjMwODc5Mw@@._V1_SX300.jpg",
                },
                new Movie()
                {
                    Id = 4,
                    Title = "E",
                    Director = "Test Director 4",
                    ReleaseYear = 2021,
                    Genre = Genre.Experimental,
                    Poster = "",
                },
                new Movie()
                {
                    Id = 5,
                    Title = "F",
                    Director = "Test Director 5",
                    ReleaseYear = 2021,
                    Genre = Genre.Experimental,
                    DeletedBy = new MovieDeletedBy(),
                    Poster = "https://m.media-amazon.com/images/M/MV5BYjEzN2FlYmYtNDkwMC00NGFkLWE5ODctYmE5NmYxNzE2MmRiXkEyXkFqcGdeQXVyMjMwODc5Mw@@._V1_SX300.jpg",
                },
            };

            Setup(mock => mock.Create(It.IsAny<Movie>()))
            .Returns<Movie>(m => m);

            Setup(mock => mock.GetById(It.IsAny<int>()))
            .Returns<int>((id) =>
            {
                testMovie.Id = id; //you will always get your movie
                return testMovie;
            });

            Setup(m => m.GetAll())
            .Returns(() => testMovieList);

            Setup(mock => mock.Update(It.IsAny<Movie>()))
            .Returns<Movie>(m => m);

            Setup(mock => mock.DeleteById(It.IsAny<int>()))
            .Returns<int>(id =>
            {
                testMovie.Id = id;
                return testMovie;
            });

            Setup(mock => mock.GetCreatedBy(It.IsAny<Movie>()))
            .Returns<Movie>(m => m.CreatedBy);

            Setup(mock => mock.GetEditedBy(It.IsAny<Movie>()))
            .Returns<Movie>(m => m.EditedBy);

            Setup(mock => mock.GetDeletedBy(It.IsAny<Movie>()))
            .Returns<Movie>(m => m.DeletedBy);
        }
    }
}
