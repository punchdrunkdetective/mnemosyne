﻿using System.ComponentModel.DataAnnotations;
using Xunit;

namespace mnemosyne.web.tests.ExtensionTests
{
    public class EnumExtensionTests
    {
        private enum TestEnum
        {
            [Display(Name = "Display Name")]
            WithDisplay,
            WithoutDisplay,
            [Display()]
            WithEmptyDisplay
        }

        [Fact]
        public void GetDisplayName_WithDisplayAttribute_ShouldReturnDisplayName()
        {
            var value = TestEnum.WithDisplay;
            var name = value.GetDisplayName();
            Assert.Equal("Display Name", name);
        }

        [Fact]
        public void GetDisplayName_WithoutDisplayAttribute_ShouldReturnEnumName()
        {
            var value = TestEnum.WithoutDisplay;
            var name = value.GetDisplayName();
            Assert.Equal("WithoutDisplay", name);
        }

        [Fact]
        public void GetDisplayName_WithEmptyDisplayAttribute_ShouldReturnEnumName()
        {
            var value = TestEnum.WithEmptyDisplay;
            var name = value.GetDisplayName();
            Assert.Equal("WithEmptyDisplay", name);
        }
    }
}
