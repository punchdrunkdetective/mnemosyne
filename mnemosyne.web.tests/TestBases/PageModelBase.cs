﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using mnemosyne.web.tests.Mocks;
using Moq;

namespace mnemosyne.web.tests.TestBases
{
    public class PageModelTestBase
    {
        protected readonly DefaultHttpContext _httpContext;
        protected readonly ModelStateDictionary _modelState;
        protected readonly ActionContext _actionContext;
        protected readonly EmptyModelMetadataProvider _modelMetadataProvider;
        protected readonly ViewDataDictionary _viewData;
        protected readonly TempDataDictionary _tempData;
        protected readonly PageContext _pageContext;

        protected readonly UserManagerMock _userManagerMock;
        protected readonly PasswordManagerMock _passwordManagerMock;
        protected readonly UserDataMock _userDataMock;
        protected readonly MovieDataMock _movieDataMock;
        protected readonly RatingDataMock _ratingDataMock;

        public PageModelTestBase()
        {
            _httpContext = new DefaultHttpContext();
            _modelState = new ModelStateDictionary();
            _actionContext = new ActionContext(_httpContext, new RouteData(), new PageActionDescriptor(), _modelState);
            _modelMetadataProvider = new EmptyModelMetadataProvider();
            _viewData = new ViewDataDictionary(_modelMetadataProvider, _modelState);
            _tempData = new TempDataDictionary(_httpContext, Mock.Of<ITempDataProvider>());

            _pageContext = new PageContext(_actionContext)
            {
                ViewData = _viewData,
            };

            _userManagerMock = new UserManagerMock();
            _passwordManagerMock = new PasswordManagerMock();
            _userDataMock = new UserDataMock();
            _movieDataMock = new MovieDataMock();
            _ratingDataMock = new RatingDataMock();
        }
    }
}
