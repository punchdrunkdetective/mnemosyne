﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.muse;
using mnemosyne.web.Pages.Movies;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.web.tests.MovieTests
{
    public class EditModelTests : PageModelTestBase
    {
        private readonly EditModel _editModel;

        public EditModelTests() : base()
        {
            _editModel = new EditModel(_userManagerMock.Object, _movieDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
            };
        }

        #region GET

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReadMovie()
        {
            var movieId = 1;
            _editModel.OnGet(movieId);
            _movieDataMock.Verify(m => m.GetById(movieId), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldSetMovie()
        {
            var movieId = 1;
            _editModel.OnGet(movieId);
            TestHelper.AssertEqual(_movieDataMock.testMovie, _editModel.Movie);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldSetMovieTitle()
        {
            var movieId = 1;
            _editModel.OnGet(movieId);
            Assert.Equal(_movieDataMock.testMovie.Title, _editModel.MovieTitle);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReturnPageResult()
        {
            var movieId = 1;
            var result = _editModel.OnGet(movieId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithMovieDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "Movie has been previously deleted.";
            var movieId = 1;
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy();

            _editModel.OnGet(movieId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidMovieId_ShouldRedirectToNotFoundPage()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _editModel.OnGet(movieId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullMovie()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _editModel.OnGet(movieId);

            Assert.Null(_editModel.Movie);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullMovieTitle()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _editModel.OnGet(movieId);

            Assert.Null(_editModel.MovieTitle);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movie.";
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _editModel.OnGet(movieId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _editModel.OnGet(movieId);

            Assert.IsType<PageResult>(result);
        }

        #endregion

        #region POST

        [Fact]
        public void OnPost_WithValidModelState_ShouldUpdateMovie()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _editModel.OnPost();
            _movieDataMock.Verify(m => m.Update(_editModel.Movie), Times.Once);
            _movieDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetEditedByUser()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _editModel.OnPost();
            Assert.Equal(_userManagerMock.actorId, _editModel.Movie.EditedBy.UserId); //TODO: should this assert against _testMovie instead of Movie?
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetEditedByDate()
        {
            var now = DateTime.Now;
            _editModel.Movie = _movieDataMock.testMovie;
            _editModel.OnPost();
            TestHelper.AssertEqual(now, _editModel.Movie.EditedBy.Date); //TODO: should this assert against _testMovie instead of Movie?
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetTempData()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _editModel.OnPost();
            Assert.Equal("Movie Saved", _tempData["Message"]);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldRedirectToDetailsPage()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            var result = _editModel.OnPost();

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./Details", redirectResult.PageName);

            IDictionary<string, object> routeValues = redirectResult.RouteValues;
            Assert.Single(routeValues);

            var movieId = Assert.Contains("movieId", routeValues);
            Assert.Equal(_editModel.Movie.Id, movieId);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotUpdateMovie()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _modelState.AddModelError("Test Key", "Test Message");
            _editModel.OnPost();
            _movieDataMock.Verify(m => m.Update(_editModel.Movie), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotSetTempData()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _modelState.AddModelError("Test Key", "Test Message");
            _editModel.OnPost();
            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotSetErrorMessage()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _modelState.AddModelError("Test Key", "Test Message");
            _editModel.OnPost();
            Assert.Null(_viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldReturnPageResult()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _modelState.AddModelError("Test Key", "Test Message");
            var result = _editModel.OnPost();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithUpdateError_ShouldSetErrorMessage()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            const string errMsg = "Internal Error: Error updating movie.";
            _movieDataMock
                .Setup(m => m.Update(It.IsAny<Movie>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _editModel.OnPost();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithUpdateError_ShouldNotSetTempData()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _movieDataMock
                .Setup(m => m.Update(It.IsAny<Movie>()))
                .Throws<Exception>();

            _editModel.OnPost();

            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithUpdateError_ShouldReturnPageResult()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _movieDataMock
                .Setup(m => m.Update(It.IsAny<Movie>()))
                .Throws<Exception>();

            var result = _editModel.OnPost();

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldNotUpdateMovie()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            _editModel.OnPost();

            _movieDataMock.Verify(m => m.Update(_editModel.Movie), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldSetErrorMessage()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            const string errMsg = "Can not update deleted movie.";
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            _editModel.OnPost();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldNotSetTempData()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            _editModel.OnPost();

            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldReturnPageResult()
        {
            _editModel.Movie = _movieDataMock.testMovie;
            _movieDataMock
                .Setup(m => m.GetDeletedBy(It.IsAny<Movie>()))
                .Returns<Movie>(m =>
                {
                    //if DeletedBy exists, then Movie has been deleted
                    m.DeletedBy = new MovieDeletedBy();
                    return m.DeletedBy;
                });

            var result = _editModel.OnPost();

            Assert.IsType<PageResult>(result);
        }
        #endregion
    }
}
