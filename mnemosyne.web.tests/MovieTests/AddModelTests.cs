﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.muse;
using mnemosyne.web.Pages.Movies;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.web.tests.MovieTests
{
    public class AddModelTests : PageModelTestBase
    {
        private readonly AddModel _addModel;

        public AddModelTests() : base()
        {
            //NOTE: You are not testing any bindings or validations that would be triggered by binding. This is
            //      purely server side testing, within your page model. 
            _addModel = new AddModel(_userManagerMock.Object, _movieDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
                Movie = new Movie() //TODO: should this reference the base _testMovie? that asserts can be off _testMovie instead of Movie
                {
                    Id = 1,
                    Title = "Test Title",
                    Director = "Test Director",
                    ReleaseYear = 2021,
                    Genre = Genre.Experimental,
                    Poster = "https://m.media-amazon.com/images/M/MV5BYjEzN2FlYmYtNDkwMC00NGFkLWE5ODctYmE5NmYxNzE2MmRiXkEyXkFqcGdeQXVyMjMwODc5Mw@@._V1_SX300.jpg",
                },
            };
        }

        [Fact]
        public void OnGet_ShouldReturnPageResult()
        {
            _addModel.Movie = null; //reset from constructor setup
            var result = _addModel.OnGet();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldCreateMovie()
        {
            _addModel.OnPost();
            _movieDataMock.Verify(m => m.Create(_addModel.Movie), Times.Once);
            _movieDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetCreatedByDate()
        {
            var now = DateTime.Now;
            _addModel.OnPost();
            TestHelper.AssertEqual(now, _addModel.Movie.CreatedBy.Date);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetCreatedByUser()
        {
            _addModel.OnPost();
            Assert.Equal(_userManagerMock.actorId, _addModel.Movie.CreatedBy.UserId);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetTempMessage()
        {
            _addModel.OnPost();
            Assert.Equal("Movie Created", _tempData["Message"]);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouildRedirectToDetailsPage()
        {
            var result = _addModel.OnPost();

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./Details", redirectResult.PageName);

            IDictionary<string, object> routeValues = redirectResult.RouteValues;
            Assert.Single(routeValues);

            var movieId = Assert.Contains("movieId", routeValues);
            Assert.Equal(_addModel.Movie.Id, movieId);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotCreateMovie()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            _addModel.OnPost();
            _movieDataMock.Verify(m => m.Create(_addModel.Movie), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotSetTempMessage()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            _addModel.OnPost();
            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithInvalidModelState_ShouldNotSetErrorMessage()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            _addModel.OnPost();
            Assert.Null(_viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithInvalidModelStat_ShouldReturnPageResult()
        {
            _modelState.AddModelError("Test Key", "Test Message");
            var result = _addModel.OnPost();
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithCreateError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error creating movie.";
            _movieDataMock
                .Setup(m => m.Create(It.IsAny<Movie>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _addModel.OnPost();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithCreateError_ShouldNotSetTempMessage()
        {
            _movieDataMock
                .Setup(m => m.Create(It.IsAny<Movie>()))
                .Throws<Exception>();

            _addModel.OnPost();

            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithCreateError_ShouldReturnPageResult()
        {
            _movieDataMock
                .Setup(m => m.Create(It.IsAny<Movie>()))
                .Throws<Exception>();

            var result = _addModel.OnPost();

            Assert.IsType<PageResult>(result);
        }
    }
}
