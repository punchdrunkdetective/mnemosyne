﻿using mnemosyne.web.Pages.Movies;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using System.Linq;
using Xunit;

namespace mnemosyne.web.tests.MovieTests
{
    public class ListModelTests : PageModelTestBase
    {
        private readonly ListModel _listModel;

        public ListModelTests() : base()
        {
            _listModel = new ListModel(_movieDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
            };
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("Test")]
        [InlineData("B")]
        public void OnGet_ShouldHaveAllMovies(string searchTerm)
        {
            // even if search term provided on url, filtering is done client side
            _listModel.SearchTerm = searchTerm;
            _listModel.OnGet();
            _movieDataMock.Verify(m => m.GetAll(), Times.Once);
            TestHelper.AssertEqual(_movieDataMock.testMovieList.Where(m => m.DeletedBy == null), _listModel.Movies, m => m.Id);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData("Test")]
        [InlineData("B")]
        public void OnGet_ShouldOrderMoviesByTitleAsc(string searchTerm)
        {
            // even if search term provided on url, filtering is done client side
            _listModel.SearchTerm = searchTerm;
            _listModel.OnGet();
            TestHelper.AssertEqual(_movieDataMock.testMovieList.Where(m => m.DeletedBy == null).OrderBy(m => m.Title), _listModel.Movies);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldHaveEmptyMovieList()
        {
            _movieDataMock
                .Setup(m => m.GetAll())
                .Throws<Exception>();

            _listModel.OnGet();

            Assert.Empty(_listModel.Movies);
        }


        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movies.";
            _movieDataMock
                .Setup(m => m.GetAll())
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _listModel.OnGet();

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }
    }
}
