﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.muse;
using mnemosyne.web.Pages.Movies;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.MovieTests
{
    public class DetailsModelTests : PageModelTestBase
    {
        private readonly DetailsModel _detailsModel;

        public DetailsModelTests() : base()
        {
            _detailsModel = new DetailsModel(_userManagerMock.Object, _userDataMock.Object, _movieDataMock.Object, _ratingDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
            };
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReadMovie()
        {
            var movieId = 1;
            _detailsModel.OnGet(movieId);
            _movieDataMock.Verify(m => m.GetById(movieId), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReturnPageResult()
        {
            var movieId = 1;
            var result = _detailsModel.OnGet(movieId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldSetMovie()
        {
            var movieId = 1;
            _detailsModel.OnGet(movieId);
            TestHelper.AssertEqual(_movieDataMock.testMovie, _detailsModel.Movie);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReadEditedBy()
        {
            _detailsModel.OnGet(_movieDataMock.testMovie.Id);
            _movieDataMock.Verify(m => m.GetEditedBy(_detailsModel.Movie), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReadCreatedBy()
        {
            _detailsModel.OnGet(_movieDataMock.testMovie.Id);
            _movieDataMock.Verify(m => m.GetCreatedBy(_detailsModel.Movie), Times.Once);
        }

        [Fact]
        public void OnGet_WithMovieEdited_ShouldReadEditor()
        {
            var editedBy = new MovieEditedBy() { UserId = 777 };
            _movieDataMock
                .Setup(m => m.GetEditedBy(_movieDataMock.testMovie))
                .Returns(() => editedBy);

            _detailsModel.OnGet(_movieDataMock.testMovie.Id);

            _userDataMock.Verify(m => m.GetById(editedBy.UserId), Times.Once);
            Assert.NotNull(_detailsModel.Editor);
        }

        [Fact]
        public void OnGet_WithMovieNotEdited_ShouldNotReadEditor()
        {
            _detailsModel.OnGet(_movieDataMock.testMovie.Id);
            _userDataMock.Verify(m => m.GetById(It.IsAny<int>()), Times.Never); 
            Assert.Null(_detailsModel.Editor);
        }

        [Fact]
        public void OnGet_WithMovieCreated_ShouldReadCreator()
        {
            var createdBy = new MovieCreatedBy() { UserId = 777 };
            _movieDataMock
                .Setup(m => m.GetCreatedBy(_movieDataMock.testMovie))
                .Returns(() => createdBy);

            _detailsModel.OnGet(_movieDataMock.testMovie.Id);

            _userDataMock.Verify(m => m.GetById(createdBy.UserId), Times.Once);
            Assert.NotNull(_detailsModel.Creator);
        }

        [Fact]
        public void OnGet_WithMovieNotCreated_ShouldNotReadCreator()
        {
            _detailsModel.OnGet(_movieDataMock.testMovie.Id);
            _userDataMock.Verify(m => m.GetById(It.IsAny<int>()), Times.Never); 
            Assert.Null(_detailsModel.Creator);
        }

        [Fact]
        public void OnGet_WithUserRating_ShouldReadRating()
        {
            var movieId = 1;
            _detailsModel.OnGet(movieId);
            _ratingDataMock.Verify(m => m.GetById(movieId, _userManagerMock.actorId), Times.Once);
        }

        [Fact]
        public void OnGet_WithUserRating_ShouldSetRating()
        {
            var movieId = 1;
            _detailsModel.OnGet(movieId);
            TestHelper.AssertEqual(_ratingDataMock.testRating, _detailsModel.Rating);
        }

        [Fact]
        public void OnGet_WithNoUserRating_ShouldSetDefaultRating()
        {
            var movieId = 1;
            _ratingDataMock
                .Setup(m => m.GetById(movieId, _userManagerMock.actorId))
                .Returns(() => null);

            _detailsModel.OnGet(movieId);

            var emptyRating = new Rating()
            {
                UserId = _userManagerMock.actorId,
                MovieId = movieId,
                Value = 0,
            };
            TestHelper.AssertEqual(emptyRating, _detailsModel.Rating);
        }

        [Fact]
        public void OnGet_WithNoUserRating_ShouldNotCreateRating()
        {
            var movieId = 1;
            _ratingDataMock
                .Setup(m => m.GetById(movieId, _userManagerMock.actorId))
                .Returns(() => null);

            _detailsModel.OnGet(movieId);

            _ratingDataMock.Verify(m => m.Create(It.IsAny<Rating>()), Times.Never);
        }

        [Fact]
        public void OnGet_WithMovieDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "Movie has been previously deleted.";
            var movieId = 1;
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy();

            _detailsModel.OnGet(movieId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidMovieId_ShouldRedirectToNotFoundPage()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _detailsModel.OnGet(movieId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _detailsModel.OnGet(movieId);

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullMovie()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _detailsModel.OnGet(movieId);

            Assert.Null(_detailsModel.Movie);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movie.";
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _detailsModel.OnGet(movieId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }
    }
}
