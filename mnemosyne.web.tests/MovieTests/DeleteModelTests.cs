﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.muse;
using mnemosyne.web.Pages.Movies;
using mnemosyne.web.tests.Helpers;
using mnemosyne.web.tests.TestBases;
using Moq;
using System;
using Xunit;

namespace mnemosyne.web.tests.MovieTests
{
    public class DeleteModelTests : PageModelTestBase
    {
        private readonly DeleteModel _deleteModel;

        public DeleteModelTests() : base()
        {
            _deleteModel = new DeleteModel(_userManagerMock.Object, _movieDataMock.Object)
            {
                PageContext = _pageContext,
                TempData = _tempData,
            };
        }

        #region GET

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReadMovie()
        {
            var movieId = 1;
            _deleteModel.OnGet(movieId);
            _movieDataMock.Verify(m => m.GetById(movieId), Times.Once);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldSetMovieTitle()
        {
            var movieId = 1;
            _deleteModel.OnGet(movieId);
            Assert.Equal(_movieDataMock.testMovie.Title, _deleteModel.MovieTitle);
        }

        [Fact]
        public void OnGet_WithValidMovieId_ShouldReturnPageResult()
        {
            var movieId = 1;
            var result = _deleteModel.OnGet(movieId);
            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnGet_WithMovieDeleted_ShouldSetErrorMessage()
        {
            const string errMsg = "Movie already deleted.";
            var movieId = 1;
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy();

            _deleteModel.OnGet(movieId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithInvalidMovieId_ShouldRedirectToNotFoundPage()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _deleteModel.OnGet(movieId);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetNullMovieTitle()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            _deleteModel.OnGet(movieId);

            Assert.Null(_deleteModel.MovieTitle);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error finding movie.";
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _deleteModel.OnGet(movieId);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnGet_WithReadError_ShouldReturnPageResult()
        {
            var movieId = 1;
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Throws<Exception>();

            var result = _deleteModel.OnGet(movieId);

            Assert.IsType<PageResult>(result);
        }

        #endregion

        #region POST

        [Fact]
        public void OnPost_WithValidModelState_ShouldDeleteMovie()
        {
            _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            _movieDataMock.Verify(m => m.Update(_movieDataMock.testMovie), Times.Once);
            _movieDataMock.Verify(m => m.Commit(), Times.Once);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetDeletedByUser()
        {
            Assert.Null(_movieDataMock.testMovie.DeletedBy);
            _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            Assert.Equal(_userManagerMock.actorId, _movieDataMock.testMovie.DeletedBy.UserId);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetDeletedByDate()
        {
            Assert.Null(_movieDataMock.testMovie.DeletedBy);
            var now = DateTime.Now;
            _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            TestHelper.AssertEqual(now, _movieDataMock.testMovie.DeletedBy.Date);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldSetTempData()
        {
            _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            Assert.Equal($"{_movieDataMock.testMovie.Title} Deleted", _tempData["Message"]);
        }

        [Fact]
        public void OnPost_WithValidModelState_ShouldRedirectToListPage()
        {
            var result = _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./List", redirectResult.PageName);
        }

        [Fact]
        public void OnPost_WithInvalidMovieId_ShouldNotDeleteMovie()
        {
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            _movieDataMock.Verify(m => m.Update(_movieDataMock.testMovie), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithInvalidMovieid_ShouldNotSetDeletedByRecord()
        {
            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);
            Assert.Null(_movieDataMock.testMovie.DeletedBy);
        }

        [Fact]
        public void OnPost_WithInvalidMovieId_ShouldReturnToNotFoundPage()
        {
            //TODO: consider just returning to page with an error message

            _movieDataMock
                .Setup(m => m.GetById(It.IsAny<int>()))
                .Returns(() => null);

            var result = _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            var redirectResult = Assert.IsType<RedirectToPageResult>(result);
            Assert.Equal("./NotFound", redirectResult.PageName);
        }

        [Fact]
        public void OnPost_WithDeleteError_ShouldSetErrorMessage()
        {
            const string errMsg = "Internal Error: Error deleting movie.";
            _movieDataMock
                .Setup(m => m.Update(It.IsAny<Movie>()))
                .Callback(() =>
                {
                    throw new Exception(errMsg);
                });

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithDeleteError_ShouldNotSetTempData()
        {
            _movieDataMock
                .Setup(m => m.Update(It.IsAny<Movie>()))
                .Throws<Exception>();

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithDeleteError_ShouldReturnPageResult()
        {
            _movieDataMock
                .Setup(m => m.Update(It.IsAny<Movie>()))
                .Throws<Exception>();

            var result = _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            Assert.IsType<PageResult>(result);
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldNotDeleteMovie()
        {
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy(); //if DeletedBy exists, then Movie has been deleted

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            _movieDataMock.Verify(m => m.Update(It.IsAny<Movie>()), Times.Never);
            _movieDataMock.Verify(m => m.Commit(), Times.Never);
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldSetErrorMessage()
        {
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy(); //if DeletedBy exists, then Movie has been deleted
            const string errMsg = "Movie already deleted.";

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            Assert.Equal(errMsg, _viewData["ErrorMessage"]);
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldNotSetTempData()
        {
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy(); //if DeletedBy exists, then Movie has been deleted

            _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            Assert.False(_tempData.ContainsKey("Message"));
        }

        [Fact]
        public void OnPost_WithMovieDeleted_ShouldReturnPageResult()
        {
            _movieDataMock.testMovie.DeletedBy = new MovieDeletedBy(); //if DeletedBy exists, then Movie has been deleted

            var result = _deleteModel.OnPost(_movieDataMock.testMovie.Id);

            Assert.IsType<PageResult>(result);
        }

        #endregion
    }
}
