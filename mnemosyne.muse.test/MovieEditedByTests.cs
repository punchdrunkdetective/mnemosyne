﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class MovieEditedByTests : ModelTestsBase
    {
        private readonly MovieEditedBy _testRecord;

        public MovieEditedByTests() : base()
        {
            _testRecord = new MovieEditedBy()
            {
                Id = 1,
                UserId = 1234,
                Date = DateTime.Now,
                MovieId = 7,
            };
        }

        [Fact]
        public void Constructor_ShouldSetNullMovie()
        {
            var eb = new MovieEditedBy();
            Assert.Null(eb.Movie);
        }

        [Fact]
        public void CopyConstructor_WithValidMovieEditedBy_ShouldCopyMovieEditedBy()
        {
            var copiedEditedBy = new MovieEditedBy(_testRecord);
            AssertEqual(_testRecord, copiedEditedBy);
        }

        [Fact]
        public void CopyConstructor_WithInvalidMovieEditedBy_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new MovieEditedBy(null));
        }

        [Fact]
        public void CopyConstructor_WithMovieReference_ShouldReferenceMovie()
        {
            _testRecord.Movie = new Movie();
            var copiedEditedBy = new MovieEditedBy(_testRecord);
            Assert.Same(_testRecord.Movie, copiedEditedBy.Movie);
        }

        [Fact]
        public void CopyConstructor_WithoutMovieReference_ShouldSetNullMovie()
        {
            var copiedEditedBy = new MovieEditedBy(_testRecord);
            Assert.Null(copiedEditedBy.Movie);
        }
    }
}
