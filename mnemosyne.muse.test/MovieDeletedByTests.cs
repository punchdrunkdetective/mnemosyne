﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class MovieDeletedByTests : ModelTestsBase
    {
        private readonly MovieDeletedBy _testRecord;

        public MovieDeletedByTests() : base()
        {
            _testRecord = new MovieDeletedBy()
            {
                Id = 1,
                UserId = 1234,
                Date = DateTime.Now,
                MovieId = 7,
            };
        }

        [Fact]
        public void Constructor_ShouldSetNullMovie()
        {
            var db = new MovieDeletedBy();
            Assert.Null(db.Movie);
        }

        [Fact]
        public void CopyConstructor_WithValidMovieDeletedBy_ShouldCopyMovieDeletedBy()
        {
            var copiedDeletedBy = new MovieDeletedBy(_testRecord);
            AssertEqual(_testRecord, copiedDeletedBy);
        }

        [Fact]
        public void CopyConstructor_WithInvalidMovieDeletedBy_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new MovieDeletedBy(null));
        }

        [Fact]
        public void CopyConstructor_WithMovieReference_ShouldReferenceMovie()
        {
            _testRecord.Movie = new Movie();
            var copiedDeletedBy = new MovieDeletedBy(_testRecord);
            Assert.Same(_testRecord.Movie, copiedDeletedBy.Movie);
        }

        [Fact]
        public void CopyConstructor_WithoutMovieReference_ShouldSetNullMovie()
        {
            var copiedDeletedBy = new MovieDeletedBy(_testRecord);
            Assert.Null(copiedDeletedBy.Movie);
        }
    }
}
