﻿using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.muse.test
{
    public abstract class ModelTestsBase
    {
        protected void AssertEqual(Movie expected, Movie actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Title, actual.Title);
            Assert.Equal(expected.Director, actual.Director);
            Assert.Equal(expected.ReleaseYear, actual.ReleaseYear);
            Assert.Equal(expected.Genre, actual.Genre);
            Assert.Equal(expected.Watched, actual.Watched);
        }

        protected void AssertEqual(MovieCreatedBy expected, MovieCreatedBy actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.UserId, actual.UserId);
            Assert.Equal(expected.MovieId, actual.MovieId);
            AssertEqual(expected.Date, actual.Date);
        }

        protected void AssertEqual(MovieEditedBy expected, MovieEditedBy actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.UserId, actual.UserId);
            Assert.Equal(expected.MovieId, actual.MovieId);
            AssertEqual(expected.Date, actual.Date);
        }

        protected void AssertEqual(MovieDeletedBy expected, MovieDeletedBy actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.UserId, actual.UserId);
            Assert.Equal(expected.MovieId, actual.MovieId);
            AssertEqual(expected.Date, actual.Date);
        }

        protected void AssertEqual(User expected, User actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.Username, actual.Username);
            Assert.Equal(expected.Password, actual.Password);
            Assert.Equal(expected.Role, actual.Role);
            Assert.Equal(expected.Email, actual.Email);
            Assert.Equal(expected.Surname, actual.Surname);
            Assert.Equal(expected.GivenName, actual.GivenName);
            Assert.Equal(expected.InformalName, actual.InformalName);
            AssertEqual(expected.LastLogin, actual.LastLogin);
        }

        protected void AssertEqual(UserCreatedBy expected, UserCreatedBy actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.CreatorId, actual.CreatorId);
            Assert.Equal(expected.NewUserId, actual.NewUserId);
            AssertEqual(expected.Date, actual.Date);
        }

        protected void AssertEqual(UserEditedBy expected, UserEditedBy actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.EditorId, actual.EditorId);
            Assert.Equal(expected.ModifiedUserId, actual.ModifiedUserId);
            AssertEqual(expected.Date, actual.Date);
        }

        protected void AssertEqual(UserDeletedBy expected, UserDeletedBy actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.DeletorUserid, actual.DeletorUserid);
            Assert.Equal(expected.RemovedUserId, actual.RemovedUserId);
            AssertEqual(expected.Date, actual.Date);
        }

        protected void AssertEqual(Rating expected, Rating actual)
        {
            Assert.Equal(expected.Id, actual.Id);
            Assert.Equal(expected.MovieId, actual.MovieId);
            Assert.Equal(expected.UserId, actual.UserId);
            Assert.Equal(expected.Value, actual.Value);
        }

        protected void AssertEqual(List<Rating> expected, List<Rating> actual)
        {
            Assert.Equal(expected.Count, actual.Count);
            for (var i = 0; i < actual.Count; i++)
            {
                AssertEqual(expected[i], actual[i]);
            }
        }

        protected void AssertEqual(DateTime? expectedDate, DateTime? actualDate)
        {
            Assert.Equal(expectedDate.HasValue, actualDate.HasValue);
            if (expectedDate.HasValue)
            {
                AssertEqual(expectedDate.Value, actualDate.Value);
            }
        }

        protected void AssertEqual(DateTime expectedDate, DateTime actualDate)
        {
            expectedDate = new DateTime(expectedDate.Year, expectedDate.Month, expectedDate.Day, expectedDate.Hour, expectedDate.Minute, 0);
            actualDate = new DateTime(actualDate.Year, actualDate.Month, actualDate.Day, actualDate.Hour, actualDate.Minute, 0);
            Assert.Equal(expectedDate, actualDate);
        }
    }
}
