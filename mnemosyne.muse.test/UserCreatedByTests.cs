﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class UserCreatedByTests : ModelTestsBase
    {
        private readonly UserCreatedBy _testRecord;

        public UserCreatedByTests() : base()
        {
            _testRecord = new UserCreatedBy()
            {
                Id = 1,
                Date = DateTime.Now,
                CreatorId = 1234,
                NewUserId = 253957,
                NewUser = null,
            };
        }

        [Fact]
        public void Constructor_ShouldSetNullUser()
        {
            var cb = new UserCreatedBy();
            Assert.Null(cb.NewUser);
        }

        [Fact]
        public void CopyConstructor_WithValidUserCreatedBy_ShouldCopyUserCreatedBy()
        {
            var copiedRecord = new UserCreatedBy(_testRecord);
            AssertEqual(_testRecord, copiedRecord);
        }

        [Fact]
        public void CopyConstructor_WithInvalidUserCreatedBy_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new UserCreatedBy(null));
        }

        [Fact]
        public void CopyConstructor_WithUserReference_ShouldReferenceUser()
        {
            _testRecord.NewUser = new User();
            var copiedRecord = new UserCreatedBy(_testRecord);
            Assert.Same(_testRecord.NewUser, copiedRecord.NewUser);
        }

        [Fact]
        public void CopyConstructor_WithoutUserReference_ShouldSetNullUser()
        {
            var copiedRecord = new UserCreatedBy(_testRecord);
            Assert.Null(copiedRecord.NewUser);
        }
    }
}
