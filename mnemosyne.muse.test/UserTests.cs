﻿using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.muse.test
{
    public class UserTests : ModelTestsBase
    {
        private readonly User _testUser;

        public UserTests() : base()
        {
            _testUser = new User()
            {
                Id = 1,
                Username = "username",
                Password = "password",
                Role = "Tester",
                LastLogin = DateTime.Now,
                GivenName = "given name",
                Surname = "surname",
                Email = "username@domain.com",                
            };
        }

        [Fact]
        public void Constructor_ShouldSetDefaultValues()
        {
            var u = new User();
            Assert.Equal(0, u.Id);
            Assert.Equal("", u.Username);
            Assert.Equal("", u.Password);
            Assert.Equal("", u.Role);
            Assert.Null(u.LastLogin);
            Assert.Null(u.GivenName);
            Assert.Null(u.Surname);
            Assert.Equal(u.Username, u.InformalName);
            Assert.Null(u.Email);
            Assert.Null(u.CreatedBy);
            Assert.Null(u.EditedBy);
            Assert.Null(u.DeletedBy);
        }

        [Fact]
        public void CopyConstructor_WithValidUser_ShouldCopyValues()
        {
            var copiedUser = new User(_testUser);
            AssertEqual(_testUser, copiedUser);
            // by records were originally null
            Assert.Null(copiedUser.CreatedBy);
            Assert.Null(copiedUser.EditedBy);
            Assert.Null(copiedUser.DeletedBy);
        }

        [Fact]
        public void CopyConstructor_WithInvalidUser_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new User(null));
        }

        [Fact]
        public void CopyConstructor_WithUserCreatedByReference_ShouldCopyCreatedBy()
        {
            var validCreateBy = new UserCreatedBy()
            {
                Id = 1,
                Date = DateTime.Now,
                CreatorId = 1234,
                NewUserId = _testUser.Id,
                NewUser = _testUser,
            };
            _testUser.CreatedBy = validCreateBy;

            var copiedUser = new User(_testUser);

            AssertEqual(validCreateBy, copiedUser.CreatedBy);
            Assert.Null(copiedUser.EditedBy);
            Assert.Null(copiedUser.DeletedBy);

            // test for deep copy of records
            Assert.NotSame(_testUser.CreatedBy, copiedUser.CreatedBy);
            Assert.NotSame(_testUser, copiedUser.CreatedBy.NewUser);
            Assert.Same(copiedUser, copiedUser.CreatedBy.NewUser);
        }

        [Fact]
        public void CopyConstructor_WithUserEditedByReference_ShouldCopyEditedBy()
        {
            var validEditedBy = new UserEditedBy()
            {
                Id = 1,
                Date = DateTime.Now,
                EditorId = 1234,
                ModifiedUserId = _testUser.Id,
                ModifiedUser = _testUser,
            };
            _testUser.EditedBy = validEditedBy;

            var copiedUser = new User(_testUser);

            AssertEqual(validEditedBy, copiedUser.EditedBy);
            Assert.Null(copiedUser.CreatedBy);
            Assert.Null(copiedUser.DeletedBy);

            // test for deep copy of records
            Assert.NotSame(_testUser.EditedBy, copiedUser.EditedBy);
            Assert.NotSame(_testUser, copiedUser.EditedBy.ModifiedUser);
            Assert.Same(copiedUser, copiedUser.EditedBy.ModifiedUser);
        }

        [Fact]
        public void CopyConstructor_WithUserDeletedByReference_ShouldCopyDeletedBy()
        {
            var validDeletedBy = new UserDeletedBy()
            {
                Id = 1,
                Date = DateTime.Now,
                DeletorUserid = 1234,
                RemovedUserId = _testUser.Id,
                RemovedUser = _testUser,
            };
            _testUser.DeletedBy = validDeletedBy;

            var copiedUser = new User(_testUser);

            AssertEqual(validDeletedBy, copiedUser.DeletedBy);
            Assert.Null(copiedUser.CreatedBy);
            Assert.Null(copiedUser.EditedBy);

            // test for deep copy of records
            Assert.NotSame(_testUser.DeletedBy, copiedUser.DeletedBy);
            Assert.NotSame(_testUser, copiedUser.DeletedBy.RemovedUser);
            Assert.Same(copiedUser, copiedUser.DeletedBy.RemovedUser);
        }

        [Fact]
        public void CopyConstructor_WithRatingsReference_ShouldCopyRatings()
        {
            var testRatings = new List<Rating>(new Rating[] {
                new Rating() { Id = 1, MovieId = 1, UserId = _testUser.Id },
                new Rating() { Id = 2, MovieId = 2, UserId = _testUser.Id },
                new Rating() { Id = 3, MovieId = 3, UserId = _testUser.Id },
            });
            _testUser.Ratings = testRatings;

            var copiedUser = new User(_testUser);

            AssertEqual(testRatings, copiedUser.Ratings);
            Assert.NotSame(testRatings, copiedUser.Ratings);
            Assert.True(copiedUser.Ratings.TrueForAll(r => r.User == copiedUser));
        }

        [Theory]
        [InlineData("")]
        [InlineData("username")]
        public void Username_WithValidUsername_ShouldSetUsername(string username)
        {
            var m = new User();
            m.Username = username;
            Assert.Equal(username, m.Username);
        }

        [Fact]
        public void Username_WithValidUsername_ShouldTrimValue()
        {
            var u = new User();
            u.Username = " username ";
            Assert.Equal("username", u.Username);
        }

        [Theory]
        [InlineData(null)]
        public void Username_WithInvalidUsername_ShouldThrowArguementException(string username)
        {
            var m = new User();
            var ex = Assert.Throws<ArgumentException>(() => m.Username = username);
            Assert.Equal("Username must have nonnull value.", ex.Message);
        }

        [Theory]
        [InlineData("")]
        [InlineData("password")]
        public void Password_WithValidPassword_ShouldSetPassword(string password)
        {
            var m = new User();
            m.Password = password;
            Assert.Equal(password, m.Password);
        }

        [Fact]
        public void Password_WithValidPassword_ShouldTrimValue()
        {
            var u = new User();
            u.Password = " password ";
            Assert.Equal("password", u.Password);
        }

        [Theory]
        [InlineData(null)]
        public void Password_WithInvalidPassword_ShouldThrowArguementException(string password)
        {
            var m = new User();
            var ex = Assert.Throws<ArgumentException>(() => m.Password = password);
            Assert.Equal("Password must have nonnull value.", ex.Message);
        }

        [Theory]
        [InlineData("")]
        [InlineData("Admin")]
        public void Role_WithValidRole_ShouldSetRole(string role)
        {
            var m = new User();
            m.Role = role;
            Assert.Equal(role, m.Role);
        }

        [Fact]
        public void Role_WithValidRole_ShouldTrimValue()
        {
            var u = new User();
            u.Role = " role ";
            Assert.Equal("role", u.Role);
        }

        [Theory]
        [InlineData(null)]
        public void Role_WithInvalidRole_ShouldThrowArguementException(string role)
        {
            var m = new User();
            var ex = Assert.Throws<ArgumentException>(() => m.Role = role);
            Assert.Equal("Role must have nonnull value.", ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" given name ")]
        public void GiveName_WithValidGivenName_ShouldTrimValue(string givenName)
        {
            string expectedGiveName = givenName?.Trim();
            var u = new User();
            u.GivenName = givenName;
            Assert.Equal(expectedGiveName, u.GivenName);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" some surname ")]
        public void Surname_WithValidSurname_ShouldTrimValue(string surname)
        {
            string expectedSurname = surname?.Trim();
            var u = new User();
            u.Surname = surname;
            Assert.Equal(expectedSurname, u.Surname);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" some email ")]
        public void Email_WithValidEmail_ShouldTrimValue(string email)
        {
            string expectedEmail = email?.Trim();
            var u = new User();
            u.Email = email;
            Assert.Equal(expectedEmail, u.Email);
        }

        [Theory]
        [InlineData(null, null)]
        [InlineData("", null)]
        [InlineData(" ", null)]
        [InlineData(null, "")]
        [InlineData(null, " ")]
        [InlineData("", "")]
        [InlineData(" ", " ")]
        [InlineData("", "Surname")]
        [InlineData(" ", "Surname")]
        [InlineData(null, "Surname")]
        public void InformalName_WithMissingGivenName_ShouldReturnUsername(string givenName, string surname)
        {
            _testUser.GivenName = givenName;
            _testUser.Surname = surname;
            Assert.Equal(_testUser.InformalName, _testUser.Username);
        }

        [Theory]
        [InlineData("Given Name", "")]
        [InlineData("Given Name", " ")]
        [InlineData("Given Name", null)]
        [InlineData("Given Name", "Surname")]
        public void InformalName_WithGivenName_ShouldReturnGivenName(string givenName, string surname)
        {
            _testUser.GivenName = givenName;
            _testUser.Surname = surname;
            Assert.Equal(_testUser.InformalName, _testUser.GivenName);
        }
    }
}
