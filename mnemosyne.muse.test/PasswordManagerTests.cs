﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class PasswordManagerTests
    {
        private readonly string _testPassword = "password";
        private readonly string _encodedPassword = "4oec2AzCreyosB3EVnLoPw==.DpKfS1kAzWiRoVYM8z1E3nvehkxZJO7xuggq4vKBWLU=.10000";
        private readonly PasswordManager _passwordManager;

        public PasswordManagerTests()
        {
            _passwordManager = new PasswordManager();
        }

        [Fact]
        public void EncodePassword_WithValidPassword_ShouldReturnHash()
        {
            var hash = _passwordManager.EncodePassword(_testPassword);
            Assert.False(String.IsNullOrWhiteSpace(hash));
        }

        [Fact]
        public void EncodePassword_WithValidPassword_ShouldReturnHashEncoded()
        {
            var hash = _passwordManager.EncodePassword(_testPassword);

            //hash format: {salt}.{hashedPassword}.{iterations}
            var parts = hash.Split(".");
            Assert.Equal(3, parts.Length);
            Assert.False(String.IsNullOrWhiteSpace(parts[0]));
            Assert.False(String.IsNullOrWhiteSpace(parts[1]));
            Assert.False(String.IsNullOrWhiteSpace(parts[2]));
        }

        [Fact]
        public void EncodePassword_WithNullPassword_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => _passwordManager.EncodePassword(null));
        }

        [Fact]
        public void EncodePassword_WithEmptyPassword_ShouldThrowArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _passwordManager.EncodePassword(""));
        }

        [Fact]
        public void VerifyPassword_WithValidPassword_ReturnTrue()
        {
            var valid = _passwordManager.VerifyPassword(_encodedPassword, _testPassword);
            Assert.True(valid);
        }

        [Fact]
        public void VerifyPassword_WithInvalidPassword_ReturnFalse()
        {
            var valid = _passwordManager.VerifyPassword(_encodedPassword, "wrong");
            Assert.False(valid);
        }

        [Theory]
        [InlineData("abcdefghijklmnopqrstuvwxyz")]
        [InlineData("abcdefg.hijklmnopqrs.tuvwxyz")]
        [InlineData("4oec2AzCreyosB3EVnLoPw==.abcdefg.tuvwxyz")]
        [InlineData("4oec2AzCreyosB3EVnLoPw==.DpKfS1kAzWiRoVYM8z1E3nvehkxZJO7xuggq4vKBWLU=.tuvwxyz")]
        public void VerifyPassword_WithInvalidEncodedPassword_ThrowsArgumentException(string badEncoding)
        {
            Assert.Throws<ArgumentException>(() => _passwordManager.VerifyPassword(badEncoding, _testPassword));
        }

        [Theory]
        [InlineData("encoded", null)]
        [InlineData(null, "password")]
        public void VerifyPassword_WithNullInput_ThrowsArgumentNullException(string encodedPassword, string password)
        {
            Assert.Throws<ArgumentNullException>(() => _passwordManager.VerifyPassword(encodedPassword, password));
        }

        [Theory]
        [InlineData("encoded", "")]
        [InlineData("", "password")]
        public void VerifyPassword_WithEmptyInput_ThrowsArgumentException(string encodedPassword, string password)
        {
            Assert.Throws<ArgumentException>(() => _passwordManager.VerifyPassword(encodedPassword, password));
        }
    }
}
