﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class UserDeletedByTests : ModelTestsBase
    {
        private readonly UserDeletedBy _testRecord;

        public UserDeletedByTests() : base()
        {
            _testRecord = new UserDeletedBy()
            {
                Id = 1,
                Date = DateTime.Now,
                DeletorUserid = 1234,
                RemovedUserId = 253957,
                RemovedUser = null,
            };
        }

        [Fact]
        public void Constructor_ShouldSetNullUser()
        {
            var eb = new UserEditedBy();
            Assert.Null(eb.ModifiedUser);
        }

        [Fact]
        public void CopyConstructor_WithValidUserDeletedBy_ShouldCopyUserDeletedBy()
        {
            var copiedRecord = new UserDeletedBy(_testRecord);
            AssertEqual(_testRecord, copiedRecord);
        }

        [Fact]
        public void CopyConstructor_WithInvalidUserDeletedBy_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new UserDeletedBy(null));
        }

        [Fact]
        public void CopyConstructor_WithUserReference_ShouldReferenceUser()
        {
            _testRecord.RemovedUser = new User();
            var copiedRecord = new UserDeletedBy(_testRecord);
            Assert.Same(_testRecord.RemovedUser, copiedRecord.RemovedUser);
        }

        [Fact]
        public void CopyConstructor_WithoutUserReference_ShouldSetNullUser()
        {
            var copiedRecord = new UserDeletedBy(_testRecord);
            Assert.Null(copiedRecord.RemovedUser);
        }
    }
}
