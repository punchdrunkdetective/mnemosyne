﻿using System;
using System.Collections.Generic;
using Xunit;

namespace mnemosyne.muse.test
{
    public class MovieTests : ModelTestsBase
    {
        private readonly Movie _testMovie;

        public MovieTests() : base()
        {
            _testMovie = new Movie()
            {
                Id = 1,
                Title = "Test Name",
                Director = "Test Director",
                ReleaseYear = 2021,
                Genre = Genre.Experimental, 
                Watched = true,
            };
        }

        [Fact]
        public void Constructor_ShouldSetDefaultValues()
        {
            var m = new Movie();
            Assert.Equal(0, m.Id);
            Assert.Equal("", m.Title);
            Assert.Null(m.Director);
            Assert.Null(m.ReleaseYear);
            Assert.Equal(Genre.Other, m.Genre);
            Assert.False(m.Watched);
            Assert.Null(m.CreatedBy);
            Assert.Null(m.EditedBy);
            Assert.Null(m.DeletedBy);
        }

        [Fact]
        public void CopyConstructor_WithValidMovie_ShouldCopyAllValues()
        {
            var copiedMovie = new Movie(_testMovie);
            AssertEqual(_testMovie, copiedMovie);
            Assert.Null(copiedMovie.CreatedBy);
            Assert.Null(copiedMovie.EditedBy);
            Assert.Null(copiedMovie.DeletedBy);
        }

        [Fact]
        public void CopyConstructor_WithInvalidMovie_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new Movie(null));
        }

        [Fact]
        public void CopyConstructor_WithMovieCreatedByReference_ShouldCopyCreatedBy()
        {
            var validCreateBy = new MovieCreatedBy()
            {
                Id = 1,                
                UserId = 1234,
                Date = DateTime.Now,
                MovieId = _testMovie.Id,
                Movie = _testMovie,
            };
            _testMovie.CreatedBy = validCreateBy;

            var copiedMovie = new Movie(_testMovie);

            AssertEqual(validCreateBy, copiedMovie.CreatedBy);
            Assert.Null(copiedMovie.EditedBy);
            Assert.Null(copiedMovie.DeletedBy);

            Assert.NotSame(_testMovie, copiedMovie.CreatedBy.Movie);
            Assert.Same(copiedMovie, copiedMovie.CreatedBy.Movie);
        }

        [Fact]
        public void CopyConstructor_WithMovieEditedByReference_ShouldCopyEditedBy()
        {
            var validEditedBy = new MovieEditedBy()
            {
                Id = 1,
                UserId = 1234,
                Date = DateTime.Now,
                MovieId = _testMovie.Id,
                Movie = _testMovie,
            };
            _testMovie.EditedBy = validEditedBy;

            var copiedMovie = new Movie(_testMovie);

            AssertEqual(validEditedBy, copiedMovie.EditedBy);
            Assert.Null(copiedMovie.CreatedBy);
            Assert.Null(copiedMovie.DeletedBy);

            Assert.NotSame(_testMovie, copiedMovie.EditedBy.Movie);
            Assert.Same(copiedMovie, copiedMovie.EditedBy.Movie);
        }

        [Fact]
        public void CopyConstructor_WithMovieDeletedByReference_ShouldCopyDeletedBy()
        {
            var validDeletedBy = new MovieDeletedBy()
            {
                Id = 1,
                UserId = 1234,
                Date = DateTime.Now,
                MovieId = _testMovie.Id,
                Movie = _testMovie,
            };
            _testMovie.DeletedBy = validDeletedBy;

            var copiedMovie = new Movie(_testMovie);

            AssertEqual(validDeletedBy, copiedMovie.DeletedBy);
            Assert.Null(copiedMovie.CreatedBy);
            Assert.Null(copiedMovie.EditedBy);

            Assert.NotSame(_testMovie, copiedMovie.DeletedBy.Movie);
            Assert.Same(copiedMovie, copiedMovie.DeletedBy.Movie);
        }

        [Fact]
        public void CopyConstructor_WithRatingsReference_ShouldCopyRatings()
        {
            var testRatings = new List<Rating>(new Rating[] { 
                new Rating() { Id = 1, MovieId = _testMovie.Id, UserId = 1 },
                new Rating() { Id = 2, MovieId = _testMovie.Id, UserId = 2 },
                new Rating() { Id = 3, MovieId = _testMovie.Id, UserId = 3 },
            });
            _testMovie.Ratings = testRatings;

            var copiedMovie = new Movie(_testMovie);

            AssertEqual(testRatings, copiedMovie.Ratings);
            Assert.NotSame(testRatings, copiedMovie.Ratings);
            Assert.True(copiedMovie.Ratings.TrueForAll(r => r.Movie == copiedMovie));            
        }

        [Theory]
        [InlineData("")]
        [InlineData("Test Title")]
        public void Title_WithValidTitle_ShouldSetTitle(string title)
        {
            var m = new Movie();
            m.Title = title;
            Assert.Equal(title, m.Title);
        }

        [Fact]
        public void Title_WithValidTitle_ShouldTrimValue()
        {
            var m = new Movie();
            m.Title = " some title ";
            Assert.Equal("some title", m.Title);
        }

        [Theory]
        [InlineData(null)]
        public void Title_WithInvalidTitle_ShouldThrowArguementException(string title)
        {
            var m = new Movie();
            var ex = Assert.Throws<ArgumentException>(() => m.Title = title);
            Assert.Equal("Title must have nonnull value.", ex.Message);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(" director ")]
        public void Director_WithValidDirector_ShouldTrimeValue(string director)
        {
            string expectedDirector = director?.Trim();
            var m = new Movie();
            m.Director = director;
            Assert.Equal(expectedDirector, m.Director);
        }

        [Theory]
        [InlineData(null)]
        [InlineData(0)]
        [InlineData(2021)]
        public void ReleaseYear_WithValidYear_ShouldSetYear(int? year)
        {
            var m = new Movie();
            m.ReleaseYear = year;
            Assert.Equal(year, m.ReleaseYear);
        }

        [Theory]
        [InlineData(-1)]
        public void ReleaseYear_WithInvalidYear_ShouldThrowArguementException(int? year)
        {
            var m = new Movie();
            var ex = Assert.Throws<ArgumentException>(() => m.ReleaseYear = year);
            Assert.Equal("ReleaseYear must have non negative value.", ex.Message);
        }

        [Fact]
        public void AverageRating_WithRatings_ShouldReturnAverage()
        {
            _testMovie.Ratings = new List<Rating>(new Rating[]
            {   
                new Rating() { Value = 1 },
                new Rating() { Value = 3 },
                new Rating() { Value = 4 },
                new Rating() { Value = 5 },
                new Rating() { Value = 0 },
                new Rating() { Value = 0 },
            });

            // 13/6=2.16, should truncate the rounding
            Assert.Equal(2.1, _testMovie.AverageRating);
        }

        [Fact]
        public void AverageRating_WithNoRatings_ShouldReturnZero()
        {
            _testMovie.Ratings = null;
            Assert.Equal(0, _testMovie.AverageRating);
        }

        [Fact]
        public void AverageRating_WithEmptyRatings_ShouldReturnZero()
        {
            _testMovie.Ratings = new List<Rating>();
            Assert.Equal(0, _testMovie.AverageRating);
        }

        [Fact]
        public void AverageRating_WithNullRating_ShouldExludeNullRating()
        {
            _testMovie.Ratings = new List<Rating>(new Rating[]
            {
                new Rating() { Value = 0 },
                new Rating() { Value = 1 },
                new Rating() { Value = 3 },
                null, 
                new Rating() { Value = 4 },
                new Rating() { Value = 5 },
            });

            // 13/5=2.6
            Assert.Equal(2.6, _testMovie.AverageRating);
        }
    }
}
