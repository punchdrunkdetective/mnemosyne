﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class UserEditedByTests : ModelTestsBase
    {
        private readonly UserEditedBy _testRecord;

        public UserEditedByTests() : base()
        {
            _testRecord = new UserEditedBy()
            {
                Id = 1,
                Date = DateTime.Now,
                EditorId = 1234,
                ModifiedUserId = 253957,
                ModifiedUser = null,
            };
        }

        [Fact]
        public void Constructor_ShouldSetNullUser()
        {
            var eb = new UserEditedBy();
            Assert.Null(eb.ModifiedUser);
        }

        [Fact]
        public void CopyConstructor_WithValidUserEditedBy_ShouldCopyUserEditedBy()
        {
            var copiedRecord = new UserEditedBy(_testRecord);
            AssertEqual(_testRecord, copiedRecord);
        }

        [Fact]
        public void CopyConstructor_WithInvalidUserEditedBy_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new UserEditedBy(null));
        }

        [Fact]
        public void CopyConstructor_WithUserReference_ShouldReferenceUser()
        {
            _testRecord.ModifiedUser = new User();
            var copiedRecord = new UserEditedBy(_testRecord);
            Assert.Same(_testRecord.ModifiedUser, copiedRecord.ModifiedUser);
        }

        [Fact]
        public void CopyConstructor_WithoutUserReference_ShouldSetNullUser()
        {
            var copiedRecord = new UserEditedBy(_testRecord);
            Assert.Null(copiedRecord.ModifiedUser);
        }
    }
}
