﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class MovieCreatedByTests : ModelTestsBase
    {
        private readonly MovieCreatedBy _testRecord;

        public MovieCreatedByTests() : base()
        {
            _testRecord = new MovieCreatedBy()
            {
                Id = 1,
                UserId = 1234,
                Date = DateTime.Now,
                MovieId = 7,
            };
        }

        [Fact]
        public void Constructor_ShouldSetNullMovie()
        {
            var cb = new MovieCreatedBy();
            Assert.Null(cb.Movie);
        }

        [Fact]
        public void CopyConstructor_WithValidMovieCreatedBy_ShouldCopyMovieCreatedBy()
        {
            var copiedCreatedBy = new MovieCreatedBy(_testRecord);
            AssertEqual(_testRecord, copiedCreatedBy);
        }

        [Fact]
        public void CopyConstructor_WithInvalidMovieCreatedBy_ShouldThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new MovieCreatedBy(null));
        }

        [Fact]
        public void CopyConstructor_WithMovieReference_ShouldReferenceMovie()
        {
            _testRecord.Movie = new Movie();
            var copiedCreatedBy = new MovieCreatedBy(_testRecord);
            Assert.Same(_testRecord.Movie, copiedCreatedBy.Movie);
        }

        [Fact]
        public void CopyConstructor_WithoutMovieReference_ShouldSetNullMovie()
        {
            var copiedCreatedBy = new MovieCreatedBy(_testRecord);
            Assert.Null(copiedCreatedBy.Movie);
        }
    }
}
