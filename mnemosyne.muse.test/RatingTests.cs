﻿using System;
using Xunit;

namespace mnemosyne.muse.test
{
    public class RatingTests : ModelTestsBase
    {
        private readonly Rating _testRating;

        public RatingTests() : base()
        {
            _testRating = new Rating()
            {
                Id = 1,
                MovieId = 4321,
                UserId = 1234,
                Value = 3,
                Movie = null,
                User = null,
            };
        }

        [Fact]
        public void Constructor_ShouldSetDefaultValues()
        {
            var r = new Rating();
            Assert.Equal(0, r.Id);
            Assert.Equal(0, r.MovieId);
            Assert.Equal(0, r.UserId);
            Assert.Equal(0, r.Value);
            Assert.Null(r.Movie);
            Assert.Null(r.User);
        }

        [Fact]
        public void CopyConstructor_WithValidRating_ShouldCopyAllValues()
        {
            var copiedRating = new Rating(_testRating);
            AssertEqual(_testRating, copiedRating);
        }

        [Fact]
        public void CopyConstructor_WithMovieReference_ShouldReferenceMovie()
        {
            _testRating.Movie = new Movie();
            var copiedRating = new Rating(_testRating);
            Assert.Same(_testRating.Movie, copiedRating.Movie);
        }

        [Fact]
        public void CopyConstructor_WithUserReference_ShouldReferenceUser()
        {
            _testRating.User = new User();
            var copiedRating = new Rating(_testRating);
            Assert.Same(_testRating.User, copiedRating.User);
        }

        [Fact]
        public void CopyConstructor_WithInvalidRating_ShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => new Rating(null));
        }

        [Theory]
        [InlineData(0)]
        [InlineData(3)]
        [InlineData(5)]
        public void Value_WithValidValue_ShouldSetValue(int rating)
        {
            _testRating.Value = rating;
            Assert.Equal(rating, _testRating.Value);
        }

        [Theory]
        [InlineData(-1)]
        [InlineData(6)]
        public void Value_WithInvalidValue_ShouldThrowArgumentException(int rating)
        {
            var ex = Assert.Throws<ArgumentException>(() => _testRating.Value = rating);
            Assert.Equal("Rating must be between 0 and 5.", ex.Message);
        }
    }
}
