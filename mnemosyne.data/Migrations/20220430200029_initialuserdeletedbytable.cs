﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace mnemosyne.data.Migrations
{
    public partial class initialuserdeletedbytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserDeletedBy",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DeletorUserid = table.Column<int>(type: "int", nullable: false),
                    Date = table.Column<DateTime>(type: "datetime2", nullable: false),
                    RemovedUserId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserDeletedBy", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserDeletedBy_Users_RemovedUserId",
                        column: x => x.RemovedUserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserDeletedBy_RemovedUserId",
                table: "UserDeletedBy",
                column: "RemovedUserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserDeletedBy");
        }
    }
}
