﻿using Microsoft.EntityFrameworkCore;
using mnemosyne.muse;
using System.Collections.Generic;
using System.Linq;

namespace mnemosyne.data
{
    public class SqlUserData : IUserData
    {
        private readonly MnemosyneDbContext _db;
        private readonly IPasswordManager _passwordManager;

        public SqlUserData(MnemosyneDbContext db, IPasswordManager passwordManager)
        {
            this._db = db;
            this._passwordManager = passwordManager;
        }

        public IEnumerable<User> GetAll()
        {
            return from u in _db.Users.Include(u => u.DeletedBy)
                   select u;
        }

        public User Create(User u)
        {
            _db.Add(u);
            return u;
        }

        /// <summary>
        /// Finds a User with the given username and password. Note, as a convenience for the login use case, password is assumed to be unencoded.
        /// Password will be encoded upon lookup. The User returned will be untouched. This is different from all other methods in this class, which
        /// takes password as is, i.e encoded.
        /// </summary>
        /// <param name="username">Non null username.</param>
        /// <param name="password">Unencoded, non null password.</param>
        /// <returns>User object if found, otherwise null.</returns>
        public User GetByUsernameAndPassword(string username, string password)
        {
            username = username?.Trim() ?? "";
            password = password?.Trim() ?? "";
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password)) return null;

            var user = _db.Users.Include(u => u.DeletedBy).FirstOrDefault(u => u.Username == username);
            if (user == null) return null;

            user = _passwordManager.VerifyPassword(user.Password, password) ? user : null;
            return user;
        }

        public User GetById(int id)
        {
            return _db.Users.Include(u => u.DeletedBy).FirstOrDefault(u => u.Id == id);
        }

        public User GetByEmail(string email)
        {
            email = email?.Trim() ?? ""; // email is nulleable, but "" should be unique
            return _db.Users.Include(u => u.DeletedBy).FirstOrDefault(u => u.Email == email);            
        }

        public User Update(User u)
        {
            var entity = _db.Users.Attach(u);
            entity.State = EntityState.Modified;
            return u;
        }

        public User DeleteById(int id)
        {
            var u = GetById(id); // ef updates object after SaveChanges()
            if (u != null) _db.Remove(u);
            return u;
        }

        public UserCreatedBy GetCreatedBy(User u)
        {
            _db.Users.Attach(u)
              .Reference(u => u.CreatedBy)
              .Load();

            return u.CreatedBy;
        }

        public UserEditedBy GetEditedBy(User u)
        {
            _db.Users.Attach(u)
              .Reference(u => u.EditedBy)
              .Load();

            return u.EditedBy;
        }

        public UserDeletedBy GetDeletedBy(User u)
        {
            _db.Users.Attach(u)
              .Reference(u => u.DeletedBy)
              .Load();

            return u.DeletedBy;
        }

        public bool UsernameExists(string username)
        {
            username = username?.Trim() ?? "";
            return _db.Users.Any(u => u.Username == username);
        }

        public bool EmailExists(string email)
        {
            email = email?.Trim() ?? "";
            return _db.Users.Any(u => u.Email == email);
        }

        public int Commit()
        {
            return _db.SaveChanges();
        }
    }
}
