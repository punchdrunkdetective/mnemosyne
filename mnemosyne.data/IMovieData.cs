﻿using mnemosyne.muse;
using System.Collections.Generic;

namespace mnemosyne.data
{
    public interface IMovieData
    {
        IEnumerable<Movie> GetAll();

        /// <summary>
        /// Finds movie by ID.
        /// </summary>
        /// <param name="id">Unique identifier for a movie in datastore in use.</param>
        /// <returns>Returns movie found, null otherwise.</returns>
        Movie GetById(int id);

        /// <summary>
        /// Finds all movies having input title as the title or part of the title. 
        /// </summary>
        /// <param name="title">Substring in which to search title by. If empty string or null, returns all movies.</param>
        /// <returns>Returns movies found, empty list otherwise.</returns>
        IEnumerable<Movie> GetByTitle(string title);

        /// <summary>
        /// Creates a new Movie by model.
        /// </summary>
        /// <param name="m">Movie data to persist.</param>
        /// <returns>A new Movie with a new id mapping to the data persisted.</returns>
        Movie Create(Movie m);

        /// <summary>
        /// Updates movie by model. 
        /// </summary>
        /// <param name="m">A Movie with a valid id.</param>
        /// <returns>A new Movie object with any updated state. Null if no Movie was found.</returns>
        Movie Update(Movie m);

        /// <summary>
        /// Deleted Movie by id. Completely removes the Movie record and any reference records from the database.
        /// </summary>
        /// <param name="id">Id of an existing Movie.</param>
        /// <returns>The Movie deleted or null if it can't be found.</returns>
        Movie DeleteById(int id);

        /// <summary>
        /// Explicitly loads the MovieCreatedBy record associated with the given movie. If it exists, the Movie has been created by a user. 
        /// Otherwise, the Movie was added to the system manually. 
        /// </summary>
        /// <param name="m">Already loaded Movie.</param>
        /// <returns>Return the MovieCreatedBy record owned by the given Movie. This field is also auto asigned to the Movie.</returns>
        MovieCreatedBy GetCreatedBy(Movie m);

        /// <summary>
        /// Explicitly loads the MovieEditedBy record associated with the given movie. If it exists, the Movie has been edited. Otherwise,
        /// the Movie contains it's orginal values. 
        /// </summary>
        /// <param name="m">Already loaded Movie.</param>
        /// <returns>Return the MovieEditedBy record owned by the given Movie. This field is also auto asigned to the Movie.</returns>
        MovieEditedBy GetEditedBy(Movie m);

        /// <summary>
        /// Explicitly loads the MovieDeletedBy record associated with the given movie. If it exists, the Movie is treated as "deleted" 
        /// by the system.
        /// </summary>
        /// <param name="m">Already loaded Movie.</param>
        /// <returns>Return the MovieDeletedBy record owned by the given Movie. This field is also auto asigned to the Movie.</returns>
        MovieDeletedBy GetDeletedBy(Movie m);

        /// <summary>
        /// Commit any data store state changes.
        /// </summary>
        /// <returns>Status code. 0 if successful.</returns>
        int Commit();
    }
}
