﻿using Microsoft.EntityFrameworkCore;
using mnemosyne.muse;
using System.Collections.Generic;
using System.Linq;

namespace mnemosyne.data
{
    public class SqlMovieData : IMovieData
    {
        private readonly MnemosyneDbContext db;

        public SqlMovieData(MnemosyneDbContext db)
        {
            this.db = db;
        }

        public int Commit()
        {
            return db.SaveChanges();
        }

        public Movie Create(Movie m)
        {
            db.Add(m);
            return m;
        }

        public Movie DeleteById(int id)
        {
            var m = GetById(id); // ef updates object after SaveChanges()
            if (m != null) db.Remove(m);
            return m;
        }

        public IEnumerable<Movie> GetAll()
        {
            var movies = db.Movies
                .Include(m => m.Ratings)
                .Include(m => m.DeletedBy);

            return from m in movies
                   orderby m.Title
                   select m;
        }

        public Movie GetById(int id)
        {
            //TODO: consider removing the DeletedBy include here? Do you need the optimization only for lists?
            return db.Movies.Include(m => m.DeletedBy).FirstOrDefault(m => m.Id == id);
        }

        public IEnumerable<Movie> GetByTitle(string title)
        {
            title = title?.ToLower() ?? "";
            return from m in db.Movies.Include(m => m.DeletedBy)
                   where m.Title.ToLower().Contains(title)
                   orderby m.Title
                   select m;
        }

        public MovieCreatedBy GetCreatedBy(Movie m)
        {
            db.Movies.Attach(m)
              .Reference(m => m.CreatedBy)
              .Load();

            return m.CreatedBy;
        }

        public MovieEditedBy GetEditedBy(Movie m)
        {
            db.Movies.Attach(m)
              .Reference(m => m.EditedBy)
              .Load();

            return m.EditedBy;
        }

        public MovieDeletedBy GetDeletedBy(Movie m)
        {
            db.Movies.Attach(m)
              .Reference(m => m.DeletedBy)
              .Load();

            return m.DeletedBy;
        }

        public Movie Update(Movie m)
        {
            var entity = db.Movies.Attach(m);
            entity.State = EntityState.Modified;
            return m;
        }
    }
}
