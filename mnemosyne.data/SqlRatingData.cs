﻿using Microsoft.EntityFrameworkCore;
using mnemosyne.muse;
using System.Linq;

namespace mnemosyne.data
{
    public class SqlRatingData : IRatingData
    {
        private readonly MnemosyneDbContext db;

        public SqlRatingData(MnemosyneDbContext db)
        {
            this.db = db;
        }

        public Rating GetById(int movieId, int userId)
        {
            return this.db.Ratings.FirstOrDefault(r => r.MovieId == movieId && r.UserId == userId);
        }

        public Rating Create(Rating r)
        {
            db.Add(r);
            return r;
        }

        public Rating Update(Rating r)
        {
            var entity = db.Ratings.Attach(r);
            entity.State = EntityState.Modified;
            return r;
        }

        public int Commit()
        {
            return db.SaveChanges();
        }
    }
}
