﻿using mnemosyne.muse;
using System.Collections.Generic;

namespace mnemosyne.data
{
    public interface IUserData
    {
        public IEnumerable<User> GetAll();

        /// <summary>
        /// Creates a new User by model.
        /// </summary>
        /// <param name="u">User data to persist.</param>
        /// <returns>A new User with a new id mapping to the data persisted.</returns>
        public User Create(User u);

        /// <summary>
        /// Finds a User by given username or password. Otherwise, returns null.
        /// </summary>
        public User GetByUsernameAndPassword(string username, string password);

        /// <summary>
        /// Finds user by id.
        /// </summary>
        /// <param name="id">Unique identifier for a user in datastore in use.</param>
        /// <returns>Returns user found, null otherwise.</returns>
        public User GetById(int id);

        /// <summary>
        /// Finds a User by email. Otherwise, returns null.
        /// </summary>
        public User GetByEmail(string email);

        /// <summary>
        /// Updates user by model. 
        /// </summary>
        /// <param name="u">A user with a valid id.</param>
        /// <returns>A user object with any updated state.</returns>
        User Update(User u);

        /// <summary>
        /// Deleted user by id. Completely removes the User record and any reference records from the database.
        /// </summary>
        /// <param name="id">Id of an existing user.</param>
        /// <returns>The user deleted or null if it can't be found.</returns>
        User DeleteById(int id);

        /// <summary>
        /// Explicitly loads the UserCreatedBy record associated with the given user. If it exists, the User has been created by another user.
        /// Otherwise, the User has been added to the system manually.
        /// </summary>
        /// <param name="u">Already loaded user.</param>
        /// <returns>Return the UserCreatedBy record owned by the given user. This field is also auto asigned to the user.</returns>
        UserCreatedBy GetCreatedBy(User u);

        /// <summary>
        /// Explicitly loads the UserEditedBy record associated with the given user. If it exists, the User has been edited. Otherwise,
        /// the User contains it's orginal values.
        /// </summary>
        /// <param name="u">Already loaded user.</param>
        /// <returns>Return the UserEditedBy record owned by the given user. This field is also auto asigned to the user.</returns>
        UserEditedBy GetEditedBy(User u);

        /// <summary>
        /// Explicitly loads the UserDeletedBy record associated with the given user. If it exists, the User is treated as "deleted" 
        /// by the system.
        /// </summary>
        /// <param name="u">Already loaded User.</param>
        /// <returns>Return the UserDeletedBy record owned by the given User. This field is also auto asigned to the User.</returns>
        UserDeletedBy GetDeletedBy(User u);

        /// <summary>
        /// Checks if the given user name exists in the database. 
        /// </summary>
        bool UsernameExists(string username);

        /// <summary>
        /// Checks if the given email exists in the database. 
        /// </summary>
        bool EmailExists(string email);

        /// <summary>
        /// Commit any data store state changes.
        /// </summary>
        /// <returns>Status code. 0 if successful.</returns>
        int Commit();
    }
}
