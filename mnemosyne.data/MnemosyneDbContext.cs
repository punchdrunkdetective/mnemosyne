﻿using Microsoft.EntityFrameworkCore;
using mnemosyne.muse;

namespace mnemosyne.data
{
    public class MnemosyneDbContext : DbContext
    {
        public MnemosyneDbContext(DbContextOptions<MnemosyneDbContext> options) : base(options)
        {
        }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        public DbSet<MovieCreatedBy> MovieCreatedBy { get; set; }

        public DbSet<MovieEditedBy> MovieEditedBy { get; set; }
    }
}
