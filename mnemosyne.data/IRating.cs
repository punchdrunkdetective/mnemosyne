﻿using mnemosyne.muse;

namespace mnemosyne.data
{
    public interface IRatingData
    {
        /// <summary>
        /// Finds rating by composite id.
        /// </summary>
        /// <param name="movieId">Unique identifier for a movie.</param>
        /// <param name="userId">Unique identifier for a user.</param>
        /// <returns>Returns rating found, null otherwise.</returns>
        Rating GetById(int movieId, int userId);

        /// <summary>
        /// Creates a new Rating by model.
        /// </summary>
        /// <param name="r">Rating data to persist.</param>
        /// <returns>A new Rating. Note composite key on existing movie and user.</returns>
        Rating Create(Rating r);

        /// <summary>
        /// Updates Rating by model. 
        /// </summary>
        /// <param name="r">A Rating with valid ids.</param>
        /// <returns>A new Rating object with any updated state. Null if no Rating was found.</returns>
        Rating Update(Rating r);

        /// <summary>
        /// Commit any data store state changes.
        /// </summary>
        /// <returns>Status code. 0 if successful.</returns>
        int Commit();
    }
}
