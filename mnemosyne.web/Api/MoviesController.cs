﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.data;
using mnemosyne.muse;
using System;

namespace mnemosyne.web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : Controller
    {
        private readonly IMovieData _data;

        public MoviesController(IMovieData data)
        {
            _data = data;
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteMovie(int id)
        {
            try
            {
                var movie = _data.GetById(id);
                if (movie == null)
                {
                    return NotFound();
                }

                _data.GetDeletedBy(movie);
                if (movie.DeletedBy != null)
                {
                    return BadRequest();
                }

                movie.DeletedBy = new MovieDeletedBy()
                {
                    UserId = -1, //TODO: implement api user authentication 
                    Date = DateTime.Now,
                };
                _data.Update(movie);

                _data.Commit();
                return Ok();
            }
            catch (Exception) //TODO: do something with this exception
            {
                return StatusCode(500);
            }

        }

        [HttpPost("{id}/watched/{watched}")]
        public IActionResult MarkeMovieWatched(int id, bool watched)
        {
            try
            {
                var movie = _data.GetById(id);
                if (movie == null)
                {
                    return NotFound();   
                }

                _data.GetDeletedBy(movie);
                if (movie.DeletedBy != null)
                {
                    return BadRequest();
                }

                movie.Watched = watched;
                _data.Commit();
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }
    }
}
