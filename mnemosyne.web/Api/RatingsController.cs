﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;

namespace mnemosyne.web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class RatingsController : Controller
    {
        private readonly IUserManager userManager;
        private readonly IMovieData movieData;
        private readonly IRatingData ratingData;

        public RatingsController(IUserManager userManager, IMovieData movieData, IRatingData ratingData)
        {
            this.userManager = userManager;
            this.movieData = movieData;
            this.ratingData = ratingData;
        }

        [HttpPost("RateMovie/{movieId}/rating/{rating}")]
        public IActionResult RateMovie(int movieId, int rating)
        {
            try
            {
                var movie = movieData.GetById(movieId);
                if (movie == null)
                {
                    return NotFound();
                }

                if (movieData.GetDeletedBy(movie) != null)
                {
                    return BadRequest();
                }

                if (rating < 0 || rating > Rating.Max)
                {
                    return BadRequest();
                }

                var userId = userManager.GetUserId();
                var movieRating = ratingData.GetById(movieId, userId);
                if (movieRating == null)
                {
                    movieRating = new Rating()
                    {
                        MovieId = movieId,
                        UserId = userId,
                        Value = rating,
                    };
                    ratingData.Create(movieRating);
                }
                else
                {
                    movieRating.Value = rating;
                    ratingData.Update(movieRating);
                }
                
                ratingData.Commit();
                return Ok();
            }
            catch (Exception) //TODO: do something with this exception
            {
                return StatusCode(500);
            }
        }
    }
}
