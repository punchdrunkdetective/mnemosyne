﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.services;
using System.Net.Http;
using System.Threading.Tasks;

namespace mnemosyne.web.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LookupController : Controller
    {
        private readonly OmdbService _omdbService;

        public LookupController(OmdbService omdbService)
        {
            _omdbService = omdbService;
        }

        [HttpGet("FindMovie")]
        [Produces("application/json")]
        public async Task<IActionResult> FindMovieAsync(string title)
        {
            try
            {
                var data = await _omdbService.FindMovieAsync(title);
                return Ok(data);
            }
            catch (HttpRequestException ex)
            {
                return StatusCode((int)ex.StatusCode, ex.Message);
            }
        }
    }
}
