﻿using Microsoft.AspNetCore.Mvc;
using mnemosyne.data;
using System;
using System.Text.RegularExpressions;

namespace mnemosyne.web.Api
{
    [Route("api/[controller]")]
    [ApiController]        
    public class UsersController : ControllerBase
    {
        private readonly IUserData _userData;

        public UsersController(IUserData userData)
        {
            _userData = userData;
        }

        /// <summary>
        /// Verify an email is in the correct format. Does not guarantee username is unique to the system.
        /// Designed to be used with the RemoteAttribute. 
        /// Null is a valid email.
        /// </summary>
        [HttpGet("VerifyEmail")]
        [Produces("application/json")]
        public IActionResult VerifyEmail(string email)
        {
            try
            {
                //work around for form model binding auto naming fields 
                email = email == null ? Request?.Query["UserModel.Email"] : email;
                email = email?.Trim();

                if (email != null)
                {
                    if (string.IsNullOrWhiteSpace(email)) // is it whitespace? null is a valid email
                    {
                        return StatusCode(400, "Email not supplied.");
                    }

                    var correctFormat =
                        Regex.IsMatch(
                            email,
                            @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                            RegexOptions.IgnoreCase,
                            TimeSpan.FromMilliseconds(250));

                    if (!correctFormat)
                    {
                        return Ok("Invalid email.");
                    }
                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Verify a username is in the correct format. Does not guarantee username is unique to the system.
        /// Designed to be used with the RemoteAttribute. 
        /// </summary>
        [HttpGet("VerifyUsername")]
        [Produces("application/json")]
        public IActionResult VerifyUsername(string username)
        {
            try
            {
                //work around for form model binding auto naming fields 
                username = string.IsNullOrWhiteSpace(username) ? Request?.Query["UserModel.Username"] : username;
                username = username?.Trim();

                if (string.IsNullOrWhiteSpace(username))
                {
                    return StatusCode(400, "Username not supplied.");
                }

                var hasSpecial =
                    Regex.IsMatch(
                        username,
                        @"[^a-z0-9_-]",
                        RegexOptions.IgnoreCase,
                        TimeSpan.FromMilliseconds(250));

                if (hasSpecial)
                {
                    return Ok("Invalid username.");
                }

                return Ok(true);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        /// <summary>
        /// Verify a password is in the correct format. Passwords do not have to be unique to the system.
        /// Designed to be used with the RemoteAttribute. 
        /// </summary>
        [HttpGet("VerifyPassword")]
        [Produces("application/json")]
        public IActionResult VerifyPassword(string password)
        {
            try
            {
                //work around for form model binding auto naming fields 
                password = password == null? Request?.Query["UserModel.Password"] : password;

                if (password != null) // password is allowed null for edit case
                {
                    var hasSpace =
                        Regex.IsMatch(
                            password,
                            @"\s",
                            RegexOptions.IgnoreCase,
                            TimeSpan.FromMilliseconds(250));

                    if (hasSpace)
                    {
                        return Ok("Whitespace is not allowed in passwords.");
                    }

                    if (password.Length < 6)
                    {
                        return Ok("Password should be at least 6 characters long.");
                    }
                }

                return Ok(true);
            }
            catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }            
        }
    }
}
