﻿using Microsoft.Extensions.Configuration;

public static class IConfigurationExtensions
{
    public static string GetApiKey(this IConfiguration configuration, string key)
    {
        return configuration.GetSection("ApiKeys").GetValue<string>(key);
    }
}
