﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

public static class EnumExtensions
{
    public static string GetDisplayName(this Enum enumValue)
    {
        var enumType = enumValue.GetType();
        var enumName = enumType.GetEnumName(enumValue);
        var memberInfo = enumType.GetMember(enumName).First();
        return memberInfo.GetCustomAttribute<DisplayAttribute>()?.GetName() ?? enumName;
    }
}

