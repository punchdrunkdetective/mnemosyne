﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

public static class PageModelUtility
{
    public static IEnumerable<SelectListItem> GetEnumSelectList<E>() where E : Enum
    {
        return typeof(E)
            .GetEnumValues().Cast<E>()
            .Select(v =>
            {
                var n = v.GetDisplayName();
                return (n, v);
            })
            .Select(t => new SelectListItem(t.n, t.v.ToString()));
    }
}

