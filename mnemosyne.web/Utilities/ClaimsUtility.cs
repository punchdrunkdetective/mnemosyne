﻿using Microsoft.AspNetCore.Authentication.Cookies;
using mnemosyne.muse;
using System.Collections.Generic;
using System.Security.Claims;

namespace mnemosyne.web.Utilities
{
    /// <summary>
    /// Provides functionality to both access and create claims on a principle. 
    /// </summary>
    public static class ClaimsUtility
    {
        //TODO: consider folding this class into the user manager. 
        //      if every use case uses both this class and the user manager, then they should be one.

        public static ClaimsPrincipal GetPrinciple(User record)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, record.Id.ToString()),
                    new Claim(ClaimTypes.Role, record.Role),
                    new Claim(ClaimTypes.Name, record.InformalName),
                };

            var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            return new ClaimsPrincipal(identity);
        }

        public static int GetUserId(ClaimsPrincipal principal)
        {
            return int.Parse(principal.FindFirst(ClaimTypes.NameIdentifier).Value);
        }

        public static string GetRole(ClaimsPrincipal principal)
        {
            return principal.FindFirst(ClaimTypes.Role).Value;
        }

        public static string GetName(ClaimsPrincipal principal)
        {
            return principal.FindFirst(ClaimTypes.Name).Value;
        }

        public static bool ValidClaims(ClaimsPrincipal principal, User record)
        {
            return GetUserId(principal) == record.Id &&
                   GetRole(principal) == record.Role &&
                   GetName(principal) == record.InformalName;
        }
    }
}
