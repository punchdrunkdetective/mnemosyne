using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;
using System.ComponentModel.DataAnnotations;

namespace mnemosyne.web.Pages.Profiles
{
    public class EditModel : PageModel
    {
        //TODO: figure a was to use User model, having to recreate User properties here because of User.Password required constraint 

        private readonly IUserManager _userManager;
        private readonly IPasswordManager _passwordManager;
        private readonly IUserData _userData;

        [Required]
        [BindProperty]
        [Remote(action: "VerifyUsername", controller: "Users")]
        public string Username { get; set; }

        [BindProperty]
        [DataType(DataType.Password)]
        [Remote(action: "VerifyPassword", controller: "Users")]
        public string Password { get; set; }

        [BindProperty]
        [DisplayFormat(ConvertEmptyStringToNull = false)] // null will blow up the policy code
        public string Role { get; set; }

        [BindProperty]
        [Display(Name = "First Name")]
        public string GivenName { get; set; }

        [BindProperty]
        [Display(Name = "Last Name")]
        public string Surname { get; set; }

        [BindProperty]
        [Remote(action: "VerifyEmail", controller: "Users")]
        public string Email { get; set; }

        public EditModel(IUserManager userManager, IPasswordManager passwordManager, IUserData userData)
        {
            _userManager = userManager;
            _passwordManager = passwordManager;
            _userData = userData;
        }

        public IActionResult OnGet()
        {
            try
            {
                var userId = _userManager.GetUserId();
                var data = _userData.GetById(userId);

                if (data == null)
                {
                    return RedirectToPage("./NotFound");
                }

                Username = data.Username;
                Password = null; // password handled manually, not required on edits
                Role = data.Role;
                GivenName = data.GivenName;
                Surname = data.Surname;
                Email = data.Email;

                //safe to still show user data
                if (data.DeletedBy != null)
                {
                    throw new Exception("User has been previously deleted.");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            try
            {
                var userId = _userManager.GetUserId();
                var data = _userData.GetById(userId);

                if (data.DeletedBy != null)
                {
                    throw new Exception("Can not update deleted user.");
                }

                if (Username != data.Username && _userData.UsernameExists(Username))
                {
                    throw new Exception("Username already registered.");
                }

                if (Email != data.Email && _userData.EmailExists(Email))
                {
                    throw new Exception("Email already registered.");
                }

                data.Username = Username;
                data.Password = !string.IsNullOrWhiteSpace(Password) ? _passwordManager.EncodePassword(Password) : data.Password;
                data.Role = Role;
                data.GivenName = GivenName;
                data.Surname = Surname;
                data.Email = Email;

                _userData.GetEditedBy(data);
                if (data.EditedBy == null)
                {
                    data.EditedBy = new UserEditedBy();
                }
                data.EditedBy.EditorId = _userManager.GetUserId();
                data.EditedBy.Date = DateTime.Now;

                _userData.Update(data);
                _userData.Commit();

                TempData["Message"] = "User Saved";
                return RedirectToPage("./Details");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return Page();
            }
        }
    }
}
