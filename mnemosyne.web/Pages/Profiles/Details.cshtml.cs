using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;

namespace mnemosyne.web.Pages.Profiles
{
    public class DetailsModel : PageModel
    {
        private readonly IUserManager _userManager;
        private readonly IUserData _userData;

        public User Details { get; set; }

        public DetailsModel(IUserManager userManager, IUserData userData)
        {
            _userManager = userManager;
            _userData = userData;
        }

        public IActionResult OnGet()
        {
            try
            {
                var userId = _userManager.GetUserId();
                Details = _userData.GetById(userId);

                if (Details == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (Details.DeletedBy != null)
                {
                    throw new Exception("User has been previously deleted.");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }
    }
}
