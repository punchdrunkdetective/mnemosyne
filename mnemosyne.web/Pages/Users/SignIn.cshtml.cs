using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using mnemosyne.web.Utilities;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace mnemosyne.web.Pages.Users
{
    [AllowAnonymous]
    public class SignInModel : PageModel
    {
        private readonly IUserManager _userManager;
        private readonly IUserData _userData;

        [BindProperty]
        public User UserForm { get; set; }

        public SignInModel(IUserManager userManager, IUserData userData)
        {
            _userManager = userManager;
            _userData = userData;
            UserForm = new User();
        }

        public IActionResult OnGet(string returnUrl = "/")
        {
            try
            {
                if (_userManager.IsAuthenticated()) return LocalRedirect(returnUrl);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = "/")
        {
            try
            {
                if (!ModelState.IsValid) return Page();
                if (_userManager.IsAuthenticated()) return LocalRedirect(returnUrl);

                var userRecord = _userData.GetByUsernameAndPassword(UserForm.Username, UserForm.Password);
                if (userRecord == null) return Unauthorized();
                if (userRecord.DeletedBy != null)
                {
                    throw new Exception("User has been deleted.");
                }

                var principal = ClaimsUtility.GetPrinciple(userRecord);
                await _userManager.SignIn(principal);
                TempData["SignedIn"] = true;

                userRecord.LastLogin = DateTime.Now;
                UserForm = _userData.Update(userRecord);
                _userData.Commit();

                return LocalRedirect(returnUrl);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return Page();
            }
        }

        public async Task<IActionResult> OnPostSignOutAsync(string errorUrl = "/")
        {
            try
            {
                if (_userManager.IsAuthenticated()) await _userManager.SignOut();
                return LocalRedirect("/");
            }
            catch (Exception ex)
            {
                TempData["Message"] = ex.Message;
                return LocalRedirect(errorUrl);
            }
        }
    }
}
