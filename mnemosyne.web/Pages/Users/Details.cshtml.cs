using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using System;

namespace mnemosyne.web.Pages.Users
{
    [Authorize(Policy = "IsAdmin")]
    public class DetailsModel : PageModel
    {
        private readonly IUserData _userData;

        public User Details { get; set; }

        public User Creator { get; set; }

        public User Editor { get; set; }

        public DetailsModel(IUserData userData)
        {
            _userData = userData;
        }

        public IActionResult OnGet(int userId)
        {
            try
            {
                Details = _userData.GetById(userId);

                if (Details == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (Details.DeletedBy != null)
                {
                    throw new Exception("User has been previously deleted.");
                }

                var editorId = _userData.GetEditedBy(Details)?.EditorId;
                if (editorId != null)
                {
                    Editor = _userData.GetById(editorId.Value);
                }

                var creatorId = _userData.GetCreatedBy(Details)?.CreatorId;
                if (creatorId != null)
                {
                    Creator = _userData.GetById(creatorId.Value);
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }
    }
}
