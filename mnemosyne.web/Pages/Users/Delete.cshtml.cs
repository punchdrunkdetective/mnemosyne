using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using mnemosyne.web.Utilities;
using System;

namespace mnemosyne.web.Pages.Users
{
    [Authorize(Policy = "IsAdmin")]
    public class DeleteModel : PageModel
    {
        private readonly IUserManager _userManager;
        private readonly IUserData _userData;

        public User DeletedUser { get; set; }

        public DeleteModel(IUserManager userManager, IUserData userData)
        {
            _userManager = userManager;
            _userData = userData;
        }

        public IActionResult OnGet(int userId)
        {
            try
            {
                DeletedUser = _userData.GetById(userId);

                if (DeletedUser == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (DeletedUser.DeletedBy != null)
                {
                    throw new Exception("User already deleted.");
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }

        public IActionResult OnPost(int userId)
        {
            try
            {
                if (userId == _userManager.GetUserId())
                {
                    throw new Exception("Error: Cannot delete yourself.");
                }

                DeletedUser = _userData.GetById(userId);

                if (DeletedUser == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (DeletedUser.DeletedBy != null)
                {
                    throw new Exception("User already deleted.");
                }

                DeletedUser.DeletedBy = new UserDeletedBy()
                {
                    DeletorUserid = _userManager.GetUserId(),
                    Date = DateTime.Now,
                };

                _userData.Update(DeletedUser);
                _userData.Commit();
                TempData["Message"] = $"{DeletedUser.Username} Deleted";
                return RedirectToPage("./List");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return Page();
            }
        }
    }
}
