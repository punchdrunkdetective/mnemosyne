using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace mnemosyne.web.Pages.Users
{
    [Authorize(Policy = "IsAdmin")]
    public class ListModel : PageModel
    {
        private readonly IUserData _userData;

        public IEnumerable<User> Users { get; set; }

        public ListModel(IUserData userData)
        {
            _userData = userData;
            Users = new List<User>();
        }

        public void OnGet()
        {
            try
            {
                Users = _userData.GetAll().Where(u => u.DeletedBy == null);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
        }
    }
}
