using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;

namespace mnemosyne.web.Pages.Users
{
    [Authorize(Policy = "IsAdmin")]
    public class AddModel : PageModel
    {
        private readonly IUserManager _userManager;
        private readonly IPasswordManager _passwordManager;
        private readonly IUserData _userData;

        [BindProperty]
        public User UserModel { get; set; }

        public AddModel(IUserManager userManager, IPasswordManager passwordManager, IUserData userData)
        {
            _userManager = userManager;
            _passwordManager = passwordManager;
            _userData = userData;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            try
            {
                if (_userData.UsernameExists(UserModel.Username))
                {
                    throw new Exception("Username already registered.");
                }

                if (_userData.EmailExists(UserModel.Email))
                {
                    throw new Exception("Email already registered.");
                }

                UserModel.Password = _passwordManager.EncodePassword(UserModel.Password);

                UserModel.CreatedBy = new UserCreatedBy()
                {
                    CreatorId = _userManager.GetUserId(),
                    Date = DateTime.Now,
                };

                var u = _userData.Create(UserModel);

                _userData.Commit();
                TempData["Message"] = "User Created";
                return RedirectToPage("./Details", new { userId = u.Id });
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return Page();
            }
        }
    }
}
