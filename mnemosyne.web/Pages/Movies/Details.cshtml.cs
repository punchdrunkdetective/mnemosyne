using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;

namespace mnemosyne.web.Pages.Movies
{
    public class DetailsModel : PageModel
    {
        private readonly IUserManager userManager;
        private readonly IUserData userData;
        private readonly IMovieData movieData;
        private readonly IRatingData ratingData;

        public Movie Movie { get; set; }

        [BindProperty]
        public Rating Rating { get; set; }

        public User Creator { get; set; }

        public User Editor { get; set; }

        public DetailsModel(IUserManager userManager, IUserData userData, IMovieData movieData, IRatingData ratingData)
        {
            this.userManager = userManager;
            this.userData = userData;
            this.movieData = movieData;
            this.ratingData = ratingData;
        }

        public IActionResult OnGet(int movieId)
        {
            try
            {
                Movie = movieData.GetById(movieId);
                if (Movie == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (Movie.DeletedBy != null)
                {
                    throw new Exception("Movie has been previously deleted.");
                }

                var editorId = movieData.GetEditedBy(Movie)?.UserId;
                if (editorId != null)
                {
                    Editor = userData.GetById(editorId.Value);
                }

                var creatorId = movieData.GetCreatedBy(Movie)?.UserId;
                if (creatorId != null)
                {
                    Creator = userData.GetById(creatorId.Value);
                }

                var userId = userManager.GetUserId();
                Rating = ratingData.GetById(movieId, userId);
                if (Rating == null)
                {
                    Rating = new Rating()
                    {
                        MovieId = movieId,
                        UserId = userId,
                        Value = 0,
                    };
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }
    }
}
