using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;

namespace mnemosyne.web.Pages.Movies
{
    public class DeleteModel : PageModel
    {
        private readonly IUserManager userManager;
        private readonly IMovieData movieData;

        [BindProperty]
        public string MovieTitle { get; set; }

        public DeleteModel(IUserManager userManager, IMovieData movieData)
        {
            this.userManager = userManager;
            this.movieData = movieData;
        }

        public IActionResult OnGet(int movieId)
        {
            try
            {
                var movie = movieData.GetById(movieId);
                if (movie == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (movie.DeletedBy != null)
                {
                    throw new Exception("Movie already deleted.");
                }

                MovieTitle = movie.Title;
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }

        public IActionResult OnPost(int movieId)
        {
            try
            {
                var movie = movieData.GetById(movieId);
                if (movie == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (movie.DeletedBy != null)
                {
                    throw new Exception("Movie already deleted.");
                }

                movie.DeletedBy = new MovieDeletedBy()
                {
                    UserId = userManager.GetUserId(),
                    Date = DateTime.Now,
                };
                movieData.Update(movie);

                movieData.Commit();
                TempData["Message"] = $"{movie.Title} Deleted";
                return RedirectToPage("./List");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
                return Page();
            }
        }
    }
}
