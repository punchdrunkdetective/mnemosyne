using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using mnemosyne.web.Utilities;
using System;
using System.Collections.Generic;
using System.Security.Claims;

namespace mnemosyne.web.Pages.Movies
{
    public class AddModel : PageModel
    {
        private readonly IUserManager _userManager;
        private readonly IMovieData _movieData;

        [BindProperty]
        public Movie Movie { get; set; }

        public IEnumerable<SelectListItem> Genres { get; private set; }

        public AddModel(IUserManager userManager, IMovieData movieData)
        {
            _userManager = userManager;
            _movieData = movieData;
            Genres = PageModelUtility.GetEnumSelectList<Genre>();
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        public IActionResult OnPost()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = _userManager.GetUserId();
                    Movie.CreatedBy = new MovieCreatedBy()
                    {
                        UserId = userId,
                        Date = DateTime.Now,
                    };

                    var m = _movieData.Create(Movie);
                    _movieData.Commit();
                    TempData["Message"] = "Movie Created";
                    return RedirectToPage("./Details", new { movieId = m.Id });
                }
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
            return Page();
        }
    }
}
