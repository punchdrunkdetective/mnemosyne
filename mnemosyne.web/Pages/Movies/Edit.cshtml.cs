using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using mnemosyne.data;
using mnemosyne.muse;
using mnemosyne.web.Authentication;
using System;
using System.Collections.Generic;

namespace mnemosyne.web.Pages.Movies
{
    public class EditModel : PageModel
    {
        private readonly IUserManager _userManager;
        private readonly IMovieData _movieData;

        [BindProperty]
        public string MovieTitle { get; set; }

        [BindProperty]
        public Movie Movie { get; set; }

        public IEnumerable<SelectListItem> Genres { get; private set; }

        public EditModel(IUserManager userManager, IMovieData movieData)
        {
            _userManager = userManager;
            _movieData = movieData;
            Genres = PageModelUtility.GetEnumSelectList<Genre>();
        }

        public IActionResult OnGet(int movieId)
        {
            try
            {
                Movie = _movieData.GetById(movieId);
                if (Movie == null)
                {
                    return RedirectToPage("./NotFound");
                }

                if (Movie.DeletedBy != null)
                {
                    throw new Exception("Movie has been previously deleted.");
                }

                MovieTitle = Movie.Title;
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId = _userManager.GetUserId();

                    _movieData.GetDeletedBy(Movie);
                    if (Movie.DeletedBy != null)
                    {
                        throw new Exception("Can not update deleted movie.");
                    }

                    _movieData.GetEditedBy(Movie);
                    if (Movie.EditedBy == null)
                    {
                        Movie.EditedBy = new MovieEditedBy()
                        {
                            MovieId = Movie.Id,
                            Movie = Movie,
                        };
                    }
                    Movie.EditedBy.UserId = userId;
                    Movie.EditedBy.Date = DateTime.Now;

                    Movie = _movieData.Update(Movie);
                    if (Movie == null)
                    {
                        return RedirectToPage("./NotFound");
                    }

                    _movieData.Commit();
                    TempData["Message"] = "Movie Saved";
                    return RedirectToPage("./Details", new { movieId = Movie.Id });
                }  
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
          
            return Page();
        }
    }
}
