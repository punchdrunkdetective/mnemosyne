using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using mnemosyne.data;
using mnemosyne.muse;
using System;
using System.Collections.Generic;
using System.Linq;

namespace mnemosyne.web.Pages.Movies
{
    public class ListModel : PageModel
    {
        private readonly IMovieData movieData;

        [BindProperty(SupportsGet = true)]
        public string SearchTerm { get; set; }

        public IEnumerable<Movie> Movies { get; set; }

        public ListModel(IMovieData movieData)
        {
            this.movieData = movieData;
            SearchTerm = "";
            Movies = new List<Movie>();
        }

        public void OnGet()
        {
            try
            {
                Movies = movieData.GetAll().Where(m => m.DeletedBy == null).OrderBy(m => m.Title);
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = ex.Message;
            }
        }
    }
}
