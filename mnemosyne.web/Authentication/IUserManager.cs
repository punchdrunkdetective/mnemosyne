﻿using System.Security.Claims;
using System.Threading.Tasks;

namespace mnemosyne.web.Authentication
{
    public interface IUserManager
    {
        public ClaimsPrincipal GetUser();

        public int GetUserId();

        public bool IsAuthenticated();

        public Task SignIn(ClaimsPrincipal user);

        public Task SignOut();
    }
}
