﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using mnemosyne.data;
using mnemosyne.web.Utilities;
using System.Threading.Tasks;

namespace mnemosyne.web.Authentication
{
    public class CustomCookieAuthenticationEvents : CookieAuthenticationEvents
    {
        private readonly IUserData _userData;

        public CustomCookieAuthenticationEvents(IUserData userData)
        {
            _userData = userData;
        }

        public override async Task ValidatePrincipal(CookieValidatePrincipalContext context)
        {
            var userPrincipal = context.Principal;
            var userId = ClaimsUtility.GetUserId(userPrincipal);
            var userRecord = _userData.GetById(userId);

            if (userRecord.DeletedBy != null ||
                ClaimsUtility.GetRole(userPrincipal) != userRecord.Role)
            {
                context.RejectPrincipal();
                await context.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            } 
            else if (!ClaimsUtility.ValidClaims(userPrincipal, userRecord))
            {
                // refresh claims cookie
                context.ReplacePrincipal(ClaimsUtility.GetPrinciple(userRecord));
                context.ShouldRenew = true;
            }
        }
    }
}
