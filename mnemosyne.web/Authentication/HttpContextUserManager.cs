﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using mnemosyne.web.Utilities;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace mnemosyne.web.Authentication
{
    public class HttpContextUserManager : IUserManager
    {
        private readonly IHttpContextAccessor _contextAccessor;

        public HttpContextUserManager(IHttpContextAccessor contextAccessor)
        {
            _contextAccessor = contextAccessor;
        }

        public ClaimsPrincipal GetUser()
        {
            return _contextAccessor.HttpContext.User;
        }

        public int GetUserId()
        {
            return ClaimsUtility.GetUserId(GetUser());
        }

        public bool IsAuthenticated()
        {
            return GetUser().Identity.IsAuthenticated;
        }

        public async Task SignIn(ClaimsPrincipal user)
        {
            await _contextAccessor.HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme,
                user,
                new AuthenticationProperties()
                {
                    //TODO: change this to be editable through remember me checkbox (isPersistent: false, exirey: n/a)
                    IsPersistent = true, // not bound to session
                    ExpiresUtc = DateTimeOffset.UtcNow.AddHours(12), // expired after 24 hours of not using the website
                });
        }

        public async Task SignOut()
        {
            await _contextAccessor.HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }
    }
}
