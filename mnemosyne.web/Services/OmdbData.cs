﻿namespace mnemosyne.services
{
    public class OmdbData
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string Year { get; set; }
        public string Director { get; set; }        
        public string Poster { get; set; }
    }
}
