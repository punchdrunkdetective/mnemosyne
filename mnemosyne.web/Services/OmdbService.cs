﻿using Microsoft.Extensions.Configuration;
using mnemosyne.muse;
using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace mnemosyne.services
{
    public class OmdbService
    {
        private readonly string _apiKey;
        private readonly HttpClient _httpClient;

        public OmdbService(IConfiguration configuration, HttpClient httpClient) {
            _apiKey = configuration.GetApiKey("Omdb");
            _httpClient = httpClient;
            _httpClient.BaseAddress = new Uri("http://www.omdbapi.com/");
        }

        public async Task<OmdbData> FindMovieAsync(string title)
        {
            var httpResponseMessage = await _httpClient.GetAsync($"?apiKey={_apiKey}&t={title}&type=movie&r=json&plot=short");

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var data = new OmdbData();

                var jsonStr = await httpResponseMessage.Content.ReadAsStringAsync();
                using (JsonDocument jsonDoc = JsonDocument.Parse(jsonStr))
                {
                    var root = jsonDoc.RootElement;

                    data.Title = root.TryGetProperty("Title", out JsonElement titleElement)
                        ? titleElement.GetString()
                        : "";

                    data.Genre = root.TryGetProperty("Genre", out JsonElement genreElement)
                        ? ToGenre(genreElement.GetString())
                        : Genre.Other.ToString();

                    data.Year = root.TryGetProperty("Year", out JsonElement yearElement)
                        ? yearElement.GetString()
                        : "";

                    data.Director = root.TryGetProperty("Director", out JsonElement directorElement)
                        ? directorElement.GetString()
                        : "";

                    data.Poster = root.TryGetProperty("Poster", out JsonElement posterElement)
                        ? posterElement.GetString()
                        : "";

                }

                return data;
            }
            else
            {
                throw new HttpRequestException(statusCode: httpResponseMessage.StatusCode, message: httpResponseMessage.ReasonPhrase, inner: null);
            }
        }

        private string ToGenre(string strValue)
        {
            foreach (var s in strValue.Split(','))
            {
                if(Enum.TryParse(typeof(Genre), s, out object g))
                {
                    return g.ToString();
                }
            }

            return Genre.Other.ToString();
        }
    }
}
