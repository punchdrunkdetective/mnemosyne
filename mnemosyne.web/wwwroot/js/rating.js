﻿$(document).ready(function () {
    $('.rating-radio input').change((evt) => {

        let $input = $(evt.target);

        let rating = $input.val();
        let movieId = $input.parent('.rating-radio').data('movie-id');
        let token = $("#RequestVerificationToken").val();

        $.ajax({
            method: "POST",
            url: `/api/ratings/ratemovie/${movieId}/rating/${rating}`,
            data: null,
            headers: {
                RequestVerificationToken: token,
            },
            error: function () {
                var myToastEl = document.getElementById('rating-error-toast');
                var myToast = bootstrap.Toast.getInstance(myToastEl);
                myToast.show();
            }
        });

    });
});
